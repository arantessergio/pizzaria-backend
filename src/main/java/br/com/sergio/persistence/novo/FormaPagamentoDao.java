package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.FormaPagamento;

@Stateless
public class FormaPagamentoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public FormaPagamentoDao() {
	}

	public void salvar(FormaPagamento t) {
		if (t.getId() == null) {
			this.manager.persist(t);
		} else {
			this.alterar(t);
		}
	}

	public void alterar(FormaPagamento t) {
		this.manager.merge(t);
	}

	public void remover(FormaPagamento t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<FormaPagamento> listar() {
		try {
			return this.manager.createQuery("select f from FormaPagamento f",
					FormaPagamento.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<FormaPagamento>();
		}
	}

	public FormaPagamento buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery(
							"select f from FormaPagamento f where f.id = :id",
							FormaPagamento.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
