package br.com.sergio.carrinho;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.sergio.models.Item;
import br.com.sergio.models.ItemBebida;
import br.com.sergio.models.Pedido;

@Named
@SessionScoped
public class Carrinho implements Serializable {

	private static final long serialVersionUID = -685390684201303118L;
	private List<Item> items;
	private List<ItemBebida> itemBebidas;
	private Pedido pedido;

	public Carrinho() {
		this.items = new ArrayList<Item>();
		this.itemBebidas = new ArrayList<ItemBebida>();
	}

	public void addItem(Item item) {
		item.setPedido(pedido);
		this.items.add(item);
	}

	public void addItemBebida(ItemBebida itemBebida) {
		itemBebida.setPedido(pedido);
		this.itemBebidas.add(itemBebida);
	}

	public void removerItemBebida(ItemBebida itemBebida) {
		if (itemBebidas.contains(itemBebida)) {
			this.itemBebidas.remove(itemBebida);
		}
	}

	public void removeAllItemBebidas() {
		this.itemBebidas.clear();
	}

	public void removeItem(Item item) {
		if (items.contains(item)) {
			this.items.remove(item);
		}
	}

	public void removeAllItems() {
		this.items.clear();
	}

	public List<Item> getItems() {
		return this.items;
	}

	public List<ItemBebida> getItemBebidas() {
		return itemBebidas;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

}
