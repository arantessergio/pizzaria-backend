package br.com.sergio.exception;

public class SenhaInvalidaException extends RuntimeException {

	private static final long serialVersionUID = -2525327063164859919L;

	public SenhaInvalidaException(String message) {
		super(message);
	}

}
