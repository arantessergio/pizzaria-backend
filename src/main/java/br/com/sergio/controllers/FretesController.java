package br.com.sergio.controllers;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Frete;
import br.com.sergio.models.Pessoa;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.FreteDao;

@Controller
public class FretesController {

	private FreteDao dao;
	private UsuarioLogado logado;
	private Result result;
	private BairroDao bairroDao;

	@Inject
	public FretesController(FreteDao dao, UsuarioLogado logado, Result result,
			BairroDao bairroDao) {
		super();
		this.dao = dao;
		this.logado = logado;
		this.result = result;
		this.bairroDao = bairroDao;
	}

	public FretesController() {
	}

	public void formulario() {
		inserirInformacoesCabecalho();
	}

	private void inserirInformacoesCabecalho() {
		Pessoa pessoa = this.logado.getPessoa();
		if (pessoa instanceof Atendente) {
			Atendente atendente = (Atendente) pessoa;
			if (atendente != null) {
				Long idEstabelecimento = atendente.getEstabelecimento().getId();
				this.result.include("idEstabelecimento", idEstabelecimento);
				this.result.include("estabelecimento", atendente
						.getEstabelecimento().getNomeFantasia());
				List<Bairro> listaBairros = this.bairroDao
						.buscarPorCidade(this.logado.getPessoa().getEndereco()
								.getCidade().getId());
				Collections.sort(listaBairros, new Comparator<Bairro>() {
					@Override
					public int compare(Bairro o1, Bairro o2) {
						return o1.getNome().compareTo(o2.getNome());
					}
				});
				this.result.include("bairros", listaBairros);
				this.result.include("nome", atendente.getNome());
				this.result.include("atendente", true);
			}
		} else {
			Cliente cliente = (Cliente) pessoa;
			if (cliente != null) {
				this.result.include("nome", cliente.getNome());
				this.result.include("cliente", true);
			}
		}
	}

	public void salvar(String bairro, BigDecimal valor) {
		Frete freteDb = this.dao.buscarPorBairro(Long.valueOf(bairro));
		
		Atendente pessoa = (Atendente) this.logado.getPessoa();
		Estabelecimento estabelecimento = pessoa.getEstabelecimento();
		
		if (freteDb == null) {
			Frete frete = new Frete();
			frete.setBairro(this.bairroDao.buscarPorId(Long.valueOf(bairro)));
			frete.setEstabelecimento(estabelecimento);
			frete.setValor(valor);

			this.dao.salvar(frete);
			this.result.include("success", "Frete salvo com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.dao.remover(freteDb);

			Frete frete = new Frete();
			frete.setEstabelecimento(estabelecimento);
			frete.setBairro(this.bairroDao.buscarPorId(Long.valueOf(bairro)));
			frete.setValor(valor);

			this.dao.salvar(frete);
			this.result.include("success", "Frete salvo com sucesso!");
			this.result.redirectTo(this).lista();
		}
	}

	@Path(value = "/fretes/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Frete frete = this.dao.buscarPorId(id);
		if (frete != null) {
			this.result.include("id", frete.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void confirmaexclusao() {
	}

	@Path("/fretes/excluir/{id}")
	public void excluir(Long id) {
		Frete frete = dao.buscarPorId(id);
		if (frete != null) {
			this.dao.remover(frete);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

	@Path(value = "/fretes/{id}", priority = Path.LOW)
	public void editar(Long id) {
		Frete frete = this.dao.buscarPorId(id);
		if (frete != null) {
			this.result.include(frete);
			this.result.redirectTo(this).formulario();
		} else {
			this.result.notFound();
		}
	}

	@Get("/fretes")
	public List<Frete> lista() {
		inserirInformacoesCabecalho();
		Long idEstabelecimento = ((Atendente)this.logado.getPessoa()).getEstabelecimento().getId();
		return dao.listarPorEstabelecimento(idEstabelecimento);
	}

}
