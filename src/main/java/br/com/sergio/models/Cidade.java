package br.com.sergio.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotNull;

@Entity
public class Cidade implements Serializable {

	private static final long serialVersionUID = 1354995037313464314L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	@NotNull(message = "O IBGE deve ser preenchido!")
	private String ibge;
	@NotNull(message = "O nome da cidade deve ser preenchido!")
	private String nome;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "cidade_id")
	private List<Bairro> bairros;

	@ManyToOne(cascade = CascadeType.ALL)
	private Estado estado;

	@OneToMany(cascade = CascadeType.ALL)
	@JoinColumn(name = "cidade_id")
	private List<Endereco> enderecos;

	public Cidade() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getIbge() {
		return ibge;
	}

	public void setIbge(String ibge) {
		this.ibge = ibge;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public List<Bairro> getBairros() {
		return bairros;
	}

	public void setBairros(List<Bairro> bairros) {
		this.bairros = bairros;
	}

	public Estado getEstado() {
		return estado;
	}

	public void setEstado(Estado estado) {
		this.estado = estado;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public void setEnderecos(List<Endereco> enderecos) {
		this.enderecos = enderecos;
	}

}
