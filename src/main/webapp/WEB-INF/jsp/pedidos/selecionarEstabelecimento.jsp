<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Pizza express</title>
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menuclientes.jsp" />
			<div class="col-sm-9">
				<h2>Adicionar bebida</h2>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">${ erro }</div>
					</c:if>
				</div>
				<form class="form-horizontal"
					action="${ linkTo[PedidosController].estabelecimentoSelecionado }" method="post">
					<fieldset>
						<br> Pizzaria:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="pizzaria" name="idEstabelecimento" class="form-control">
									<c:forEach items="${ estabelecimentos }" var="e">
										<option value="${ e.id }">${ e.nomeFantasia }</option>
									</c:forEach>
								</select>
							</div>
							<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="botao" class="btn btn-success"
									type="submit">Próximo</button>
							</div>
						</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>

</body>
</html>