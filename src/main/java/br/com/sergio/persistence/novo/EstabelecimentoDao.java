package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Estabelecimento;

@Stateless
public class EstabelecimentoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public EstabelecimentoDao() {
	}

	public void salvar(Estabelecimento t) {
		if (t.getId() == null)
			this.manager.persist(t);
		else
			this.alterar(t);
	}

	public void alterar(Estabelecimento t) {
		this.manager.merge(t);
	}

	public void remover(Estabelecimento t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Estabelecimento> listar() {
		try {
			return this.manager.createQuery("select e from Estabelecimento e",
					Estabelecimento.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Estabelecimento>();
		}
	}

	public Estabelecimento buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery(
							"select e from Estabelecimento e where e.id = :id",
							Estabelecimento.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Estabelecimento> buscarPorBairro(Long idBairro) {
		try {
			return this.manager
					.createQuery(
							"select e from Estabelecimento e where e.endereco.bairro.id = :id",
							Estabelecimento.class).setParameter("id", idBairro)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Estabelecimento>();
		}
	}

	public List<Estabelecimento> buscarPorCidade(Long idCidade) {
		try {
			return this.manager
					.createQuery(
							"select e from Estabelecimento e where e.endereco.cidade.id = :id",
							Estabelecimento.class).setParameter("id", idCidade)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Estabelecimento>();
		}
	}

	public List<Estabelecimento> buscarPorEstado(Long idEstado) {
		try {
			return this.manager
					.createQuery(
							"select e from Estabelecimento e where e.endereco.estado.id = :id",
							Estabelecimento.class).setParameter("id", idEstado)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Estabelecimento>();
		}
	}

}
