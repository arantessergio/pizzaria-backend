package br.com.sergio.controllers.api;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import javax.enterprise.event.Observes;
import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Options;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.HttpMethod;
import br.com.caelum.vraptor.events.VRaptorRequestStarted;
import br.com.caelum.vraptor.http.route.Router;
import br.com.caelum.vraptor.view.Results;
import br.com.sergio.annotations.Livre;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Bebida;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.EStatus;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Estado;
import br.com.sergio.models.Produto;
import br.com.sergio.models.Sabor;
import br.com.sergio.persistence.novo.AtendenteDao;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.BebidaDao;
import br.com.sergio.persistence.novo.CidadeDao;
import br.com.sergio.persistence.novo.ClienteDao;
import br.com.sergio.persistence.novo.EnderecoDao;
import br.com.sergio.persistence.novo.EstabelecimentoDao;
import br.com.sergio.persistence.novo.EstadoDao;
import br.com.sergio.persistence.novo.ProdutoDao;
import br.com.sergio.persistence.novo.SaborDao;

@Controller
public class ApiController {

	private Result result;
	private EstabelecimentoDao estabelecimentoDao;
	private EstadoDao estadoDao;
	private CidadeDao cidadeDao;
	private BairroDao bairroDao;
	private ProdutoDao produtoDao;
	private ClienteDao clienteDao;
	private SaborDao saborDao;
	private BebidaDao bebidaDao;
	private AtendenteDao atendenteDao;

	private Router router;
	private EnderecoDao enderecoDao;

	@Inject
	public ApiController(Result result, EstabelecimentoDao estabelecimentoDao,
			EstadoDao estadoDao, CidadeDao cidadeDao, BairroDao bairroDao,
			ProdutoDao produtoDao, ClienteDao clienteDao, SaborDao saborDao,
			BebidaDao bebidaDao, AtendenteDao atendenteDao, Router router,
			EnderecoDao enderecoDao) {
		this.result = result;
		this.estabelecimentoDao = estabelecimentoDao;
		this.estadoDao = estadoDao;
		this.cidadeDao = cidadeDao;
		this.bairroDao = bairroDao;
		this.produtoDao = produtoDao;
		this.clienteDao = clienteDao;
		this.saborDao = saborDao;
		this.bebidaDao = bebidaDao;
		this.atendenteDao = atendenteDao;
		this.router = router;
		this.enderecoDao = enderecoDao;
	}

	public ApiController() {
	}

	@Options
	@Path(value = "/")
	public void options(@Observes VRaptorRequestStarted requestInfo) {

		Set<HttpMethod> allowed = router.allowedMethodsFor(requestInfo
				.getRequest().getRequestedUri());
		String allowMethods = allowed.toString().replaceAll("\\[|\\]", "");

		result.use(Results.status()).header("Allow", allowMethods);
		result.use(Results.status()).header("Access-Control-Allow-Methods",
				allowMethods);
		result.use(Results.status())
				.header("Access-Control-Allow-Headers",
						"Content-Type, X-Requested-With, accept, Authorization, origin");
	}

	public void estabelecimentos() {
		List<Estabelecimento> lista = estabelecimentoDao.listar();
		this.result.use(Results.json()).withoutRoot().from(lista).serialize();
	}

	@Livre
	public void estados() {
		List<Estado> estados = estadoDao.listar();
		Collections.sort(estados, new Comparator<Estado>() {
			@Override
			public int compare(Estado o1, Estado o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.use(Results.json()).withoutRoot().from(estados).serialize();
	}

	@Livre
	public void cidades() {
		List<Cidade> cidades = cidadeDao.listar();
		Collections.sort(cidades, new Comparator<Cidade>() {
			@Override
			public int compare(Cidade o1, Cidade o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.use(Results.json()).withoutRoot().from(cidades).serialize();
	}

	@Livre
	public void bairros() {
		List<Bairro> bairros = bairroDao.listar();
		Collections.sort(bairros, new Comparator<Bairro>() {
			@Override
			public int compare(Bairro o1, Bairro o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.use(Results.json()).withoutRoot().from(bairros).serialize();
	}

	@Livre
	public void cidadesestado(Long estado) {
		List<Cidade> cidades = this.cidadeDao.buscarPorEstado(estado);
		Collections.sort(cidades, new Comparator<Cidade>() {
			@Override
			public int compare(Cidade o1, Cidade o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.use(Results.json()).from(cidades, "cidades").serialize();
	}

	public void bairroscidade(Long cidade) {
		List<Bairro> bairros = this.bairroDao.buscarPorCidade(cidade);
		Collections.sort(bairros, new Comparator<Bairro>() {
			@Override
			public int compare(Bairro o1, Bairro o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.use(Results.json()).withoutRoot().from(bairros).serialize();
	}

	@Livre
	public void estabelecimentosbairro(Long bairro) {
		List<Estabelecimento> estabelecimentos = estabelecimentoDao
				.buscarPorBairro(bairro);
		this.result.use(Results.json()).withoutRoot().from(estabelecimentos)
				.serialize();
	}

	@Livre
	public void estabelecimentoscidade(Long cidade) {
		List<Estabelecimento> estabelecimentos = estabelecimentoDao
				.buscarPorCidade(cidade);
		this.result.use(Results.json()).withoutRoot().from(estabelecimentos)
				.serialize();

	}

	@Livre
	public void estabelecimentosestado(Long estado) {
		List<Estabelecimento> estabelecimentos = estabelecimentoDao
				.buscarPorEstado(estado);
		this.result.use(Results.json()).withoutRoot().from(estabelecimentos)
				.serialize();
	}

	public void produtos() {
		List<Produto> produtos = this.produtoDao.listar();
		this.result.use(Results.json()).withoutRoot().from(produtos)
				.serialize();
	}

	public void produtosestabelecimento(Long estabelecimento) {
		List<Produto> produtos = this.produtoDao
				.findByEstabelecimento(estabelecimento);
		this.result.use(Results.json()).withoutRoot().from(produtos)
				.serialize();
	}

	@Livre
	public void login(String email, String senha) {
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
			String senhaComparar = String.format("%32x", hash);
			Atendente atendente = atendenteDao.buscarPorEmail(email);
			Cliente cliente = clienteDao.buscarPorEmail(email);
			if (atendente != null && atendente.getSenha().equals(senhaComparar)) {
				this.result.use(Results.json()).withoutRoot().from(atendente)
						.recursive().serialize();
			} else if (cliente != null
					&& cliente.getSenha().equals(senhaComparar)) {
				this.result.use(Results.json()).withoutRoot().from(cliente)
						.serialize();
			} else {
				this.result.notFound();
			}
		} catch (Exception e) {
			String erro = "{ erro : " + e.getMessage() + " }";
			this.result.use(Results.json()).withoutRoot().from(erro)
					.serialize();
			this.result.notFound();
		}
	}

	@Post
	@Livre
	public void cadastro(String nome, String telefone, String email,
			String estado, String cidade, String bairro, String senha,
			String rua, String numero, String complemento) {

		String emailMinusculo = email.toLowerCase();

		try {
			Endereco endereco = new Endereco();
			endereco.setBairro(bairroDao.buscarPorId(Long.valueOf(bairro)));
			endereco.setCidade(cidadeDao.buscarPorId(Long.valueOf(cidade)));
			endereco.setEstado(estadoDao.buscarPorId(Long.valueOf(estado)));
			endereco.setRua(rua);
			endereco.setNumero(Long.valueOf(numero));
			enderecoDao.salvar(endereco);

			MessageDigest md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
			String senhaGravar = String.format("%32x", hash);

			Cliente cliente = new Cliente();
			cliente.setEndereco(endereco);
			cliente.setNome(nome);
			cliente.setSenha(senhaGravar);
			cliente.setEmail(emailMinusculo);
			cliente.setTelefone(telefone);
			cliente.setStatus(EStatus.ATIVO);

			hash = new BigInteger(1, md.digest(emailMinusculo.getBytes()));
			String token = String.format("%32x", hash);
			cliente.setToken(token);

			clienteDao.salvar(cliente);

			Cliente clientePersistido = clienteDao.findByLogin(email, senha);
			if (clientePersistido != null) {
				this.result.use(Results.json()).withoutRoot()
						.from(clientePersistido).serialize();
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

	}

	@Post
	@Livre
	public void logout(String token) {
		Cliente cliente = this.clienteDao.buscarPorToken(token);
		if (cliente != null) {
			cliente.setToken(null);
			this.clienteDao.salvar(cliente);
			Boolean msg = Boolean.TRUE;
			this.result.use(Results.json()).withoutRoot().from(msg).serialize();
		}
	}

	/**
	 * localhost:8080/pizzaria/api/carrinho?cliente=1&produtos=1,2&quantidade=2,
	 * 2&latitude=50&longitude=50&formaPagamento=1
	 * 
	 * @Transactional public void carrinho(String cliente, String[] produtos,
	 *                String[] quantidades, String latitude, String longitude,
	 *                String formaPagamento) { Pedido pedido = new Pedido();
	 *                pedido.setAtendente(logado.getAtendente());
	 *                pedido.setCliente
	 *                (clienteDao.buscarPorId(Long.valueOf(cliente)));
	 *                pedido.setData(new Date());
	 *                pedido.setEstabelecimento(logado
	 *                .getAtendente().getEstabelecimento());
	 *                pedido.setFormaPagamento
	 *                (formaPagamentoDao.buscarPorId(Long
	 *                .valueOf(formaPagamento))); pedido.setHoraEntrega(null);
	 *                pedido.setHoraRealizacao(null); pedido.setHoraSaida(null);
	 * 
	 *                List<Item> items = new ArrayList<Item>();
	 * 
	 *                // itens for (int i = 0; i < produtos.length; i++) { Item
	 *                item = new Item(); String idProduto = produtos[i]; Produto
	 *                produto = produtoDao.buscarPorId(Long.valueOf(idProduto));
	 *                item.setProduto(produto); if (produto != null) { for (int
	 *                j = 0; j < quantidades.length; j++) { String qtde =
	 *                quantidades[j]; item.setQuantidade(Long.valueOf(qtde)); }
	 *                } items.add(item); }
	 * 
	 *                pedido.setItens(items);
	 * 
	 *                pedido.setMotoboy(null); pedido.setNumero(null);
	 *                pedido.setSituacao(ESituacaoPedido.REALIZADO);
	 * 
	 *                // total BigDecimal total = BigDecimal.ZERO; for (Item
	 *                item : items) { BigDecimal preco =
	 *                item.getProduto().getPrecoVenda(); total =
	 *                total.add(preco); }
	 * 
	 *                pedido.setTotal(total);
	 * 
	 *                pedidoDao.salvar(pedido);
	 * 
	 *                }
	 */

	/**
	 * recebe o pedido do mobile para gravar o mesmo
	 * 
	 * @param mobile
	 */
	// @Consumes
	// public void processarpedidomobile(PedidoMobile mobile) {
	// int idBebida = mobile.getIdBebida();
	// long[] idsSabores = mobile.getIdsSabores();
	// int quantidadeBebidas = mobile.getQuantidadeBebidas();
	// int quantidadePizza = mobile.getQuantidadePizza();
	// // int tamanhoPizza = mobile.getTamanhoPizza();
	// String emailCliente = mobile.getEmailCliente();
	// // double latitude = mobile.getLatitude();
	// // double longitude = mobile.getLongitude();
	// String token = mobile.getToken();
	// int idEstabelecimento = mobile.getIdEstabelecimento();
	// int idFormaPagamento = mobile.getIdFormaPagamento();
	//
	// Bebida bebida = this.bebidaDao.buscarPorId(Long.valueOf(idBebida));
	// Cliente cliente = this.clienteDao.buscarPorEmail(emailCliente);
	// Estabelecimento estabelecimento = this.estabelecimentoDao
	// .buscarPorId(Long.valueOf(idEstabelecimento));
	// FormaPagamento formaPagamento = this.formaPagamentoDao.buscarPorId(Long
	// .valueOf(idFormaPagamento));
	//
	// /** monta a partir das quantidades e ids dos sabores */
	// Item item = new Item();
	// // item.setBebida(bebida);
	// item.setQuantidade(Long.valueOf(quantidadePizza));
	// // item.setQuantidadeBebida(Long.valueOf(quantidadeBebidas));
	//
	// List<Item> items = new ArrayList<Item>();
	// items.add(item);
	//
	// List<Sabor> sabores = new ArrayList<Sabor>();
	// for (long l : idsSabores) {
	// Sabor sabor = saborDao.buscarPorId(l);
	// sabores.add(sabor);
	// }
	//
	// item.setSabores(sabores);
	//
	// /** cria o pedido */
	// Pedido pedido = new Pedido();
	// pedido.setData(new Date());
	// pedido.setEstabelecimento(estabelecimento);
	// pedido.setFormaPagamento(formaPagamento);
	// pedido.setSituacao(ESituacaoPedido.RECEBIDO);
	// pedido.setItens(items);
	// pedido.setCliente(cliente);
	//
	// /** persiste o pedido */
	// this.pedidoDao.salvar(pedido);
	//
	// /** devolve o mesmo para o cliente */
	// this.result.use(Results.json()).withoutRoot().from(pedido).serialize();
	// }

	@Livre
	public void bebidas() {
		List<Bebida> bebidas = this.bebidaDao.listar();
		this.result.use(Results.json()).withoutRoot().from(bebidas).serialize();
	}

	@Livre
	public void sabores() {
		List<Sabor> sabores = this.saborDao.listar();
		this.result.use(Results.json()).withoutRoot().from(sabores).serialize();
	}

	@Livre
	public void signupfast(String nome, String email, String senha) {

		boolean existe = validarUsuarioExistente(email);

		if (!existe) {
			Cliente cliente = new Cliente();
			cliente.setNome(nome);
			cliente.setEmail(email);

			MessageDigest md;
			try {
				md = MessageDigest.getInstance("MD5");
				BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
				String senhaSalvar = String.format("%32x", hash);
				cliente.setSenha(senhaSalvar);
				this.clienteDao.salvar(cliente);

				Cliente clienteSalvo = this.clienteDao.findByLogin(email,
						senhaSalvar);
				if (clienteSalvo != null) {
					this.result.use(Results.json()).withoutRoot()
							.from(clienteSalvo).serialize();
				} else {
					this.result.use(Results.json())
							.from("message", "erro ao salvar cliente")
							.serialize();
				}
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
			}
		} else {
			this.result
					.use(Results.json())
					.from("Já existe um usuário com este email, verifique!",
							"erro").serialize();
		}

	}

	private boolean validarUsuarioExistente(String email) {
		List<Cliente> listaClientes = this.clienteDao.listar();
		for (Iterator<Cliente> iterator = listaClientes.iterator(); iterator
				.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
			if (email.equals(cliente.getEmail())) {
				return true;
			}
		}
		return false;
	}

	@Livre
	public void perfil(String token) {
		Cliente cliente = this.clienteDao.buscarPorToken(token);
		if (cliente != null)
			this.result.use(Results.json()).withoutRoot().from(cliente)
					.serialize();
		else
			this.result.use(Results.json())
					.from("Nenhum cliente encontrado com este Token!", "erro")
					.serialize();
	}
}
