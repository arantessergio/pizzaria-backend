package br.com.sergio.models;

public enum ESituacaoPedido {

	REALIZADO {
		@Override
		public String toString() {
			return "REALIZADO";
		}
	},
	RECEBIDO {
		@Override
		public String toString() {
			return "RECEBIDO";
		}
	},
	PREPARANDO {
		@Override
		public String toString() {
			return "PREPARANDO";
		}
	},
	PRONTO {
		@Override
		public String toString() {
			return "PRONTO";
		}
	},
	SAIU_ENTREGA {
		@Override
		public String toString() {
			return "SAIU_ENTREGA";
		}
	},
	ENTREGUE {
		@Override
		public String toString() {
			return "ENTREGUE";
		}
	},
	CANCELADO {
		@Override
		public String toString() {
			return "CANCELADO";
		}
	};

}
