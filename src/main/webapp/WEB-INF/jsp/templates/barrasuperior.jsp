<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div id="top-nav" class="navbar navbar-inverse navbar-static-top">
	<div class="container-fluid">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse"
				data-target=".navbar-collapse">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="#"><c:if
					test="${ not empty estabelecimento }"> ${ estabelecimento }</c:if></a>
		</div>

		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="dropdown"><a class="dropdown-toggle" role="button"
					data-toggle="dropdown" href="#"><i class="glyphicon "></i> <c:if
							test="${ not empty nome }"> ${ nome } </c:if></a>
				<li><a href="${ linkTo[LoginController].sair }"><i
						class="glyphicon "></i>Sair</a></li>
			</ul>
		</div>
	</div>
</div>