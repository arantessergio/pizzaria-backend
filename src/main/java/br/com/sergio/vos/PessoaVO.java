package br.com.sergio.vos;

import br.com.sergio.models.EStatus;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Pessoa;

public class PessoaVO {

	private Long id;
	private String nome;
	private String rg;
	private String cpf;
	private EStatus status;
	private String login;
	private String senha;
	private Endereco endereco;

	public PessoaVO(Long id, String nome, String rg, String cpf,
			EStatus status, String login, String senha, Endereco endereco) {
		super();
		this.id = id;
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.status = status;
		this.login = login;
		this.senha = senha;
		this.endereco = endereco;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public String getRg() {
		return rg;
	}

	public String getCpf() {
		return cpf;
	}

	public EStatus getStatus() {
		return status;
	}

	public String getLogin() {
		return login;
	}

	public String getSenha() {
		return senha;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public Pessoa toPessoa(PessoaVO vo) {
		Pessoa p = new Pessoa();
		p.setCpf(vo.getCpf());
		p.setEndereco(vo.getEndereco());
		p.setId(vo.getId());
		p.setLogin(vo.getLogin());
		p.setNome(vo.getNome());
		p.setRg(vo.getRg());
		p.setSenha(vo.getSenha());
		p.setStatus(vo.getStatus());
		return p;
	}

}
