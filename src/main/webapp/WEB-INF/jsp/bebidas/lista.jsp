<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<c:if test="${ atendente }">
				<jsp:include page="../templates/menulateral.jsp" />
			</c:if>
			<c:if test="${ cliente }">
				<jsp:include page="../templates/menuclientes.jsp" />
			</c:if>
			<div class="col-sm-9">
				<h2>Bebidas</h2>
				<hr>
				<c:if test="${ atendente }">
					<div>
						<a id="btnNovo" class="btn btn-default"
							href="${ linkTo[BebidasController].formulario }">Novo</a> <a
							id="btnVisualizar" href="${ linkTo[BebidasController].lista }"
							class="btn btn-info">Visualizar</a>
					</div>
				</c:if>
				<hr>

				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Descrição</th>
							<c:if test="${ atendente }">
								<th>Preço custo</th>
							</c:if>
							<th>Preço venda</th>
							<c:if test="${ atendente }">
								<th></th>
								<th></th>
							</c:if>

						</tr>
						<c:forEach items="${ bebidaList }" var="bebida">
							<tr>
								<td><c:out value="${ bebida.descricao }" /></td>
								<c:if test="${ atendente }">
									<td><c:out value="${ bebida.valorCusto }" /></td>
								</c:if>
								<td><c:out value="${ bebida.valorVenda }" /></td>
								<c:if test="${ atendente }">
									<td><a
										href="${ linkTo[BebidasController].editar(bebida.id)}"
										class="btn btn-info">Editar</a></td>
									<td><a
										href="${ linkTo[BebidasController].solicitaexclusao(bebida.id)}"
										class="btn btn-danger">Remover</a></td>
								</c:if>
							</tr>
						</c:forEach>
					</table>
					<div class="modal-footer">
						<c:if test="${not empty erro}">
							<div class="alert alert-danger" role="alert">${ erro }</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>