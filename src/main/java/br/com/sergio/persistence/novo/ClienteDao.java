package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Cliente;

@Stateless
public class ClienteDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public ClienteDao() {
	}

	public void salvar(Cliente t) {
		if (t.getId() == null)
			this.manager.persist(t);
		else
			this.alterar(t);
	}

	public void alterar(Cliente t) {
		this.manager.merge(t);
	}

	public void remover(Cliente t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Cliente> listar() {
		try {
			return this.manager.createQuery("select c from Cliente c",
					Cliente.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Cliente>();
		}
	}

	public Cliente buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select c from Cliente c where id = :id",
							Cliente.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cliente findByLogin(String login, String senha) {
		try {
			return this.manager
					.createQuery(
							"select c from Cliente c where c.email = :login and senha = :senha",
							Cliente.class).setParameter("login", login)
					.setParameter("senha", senha).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cliente buscarPorNome(String nome) {
		try {
			return this.manager
					.createQuery(
							"select c from Cliente c where c.nome = :nome",
							Cliente.class).setParameter("nome", nome)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cliente buscarPorEmail(String email) {
		try {
			return this.manager
					.createQuery(
							"select c from Cliente c where c.email = :email",
							Cliente.class).setParameter("email", email)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cliente buscarUsuarioLoginESenha(String login, String senha) {
		try {
			return this.manager
					.createQuery(
							"select c from Cliente c where login = :login and senha = :senha",
							Cliente.class).setParameter("login", login)
					.setParameter("senha", senha).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cliente buscarPorToken(String token) {
		try {
			return this.manager
					.createQuery(
							"select c from Cliente c where c.token = :token",
							Cliente.class).setParameter("token", token)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
