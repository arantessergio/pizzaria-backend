package br.com.sergio.interceptors;

import javax.inject.Inject;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.sergio.annotations.Livre;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.controllers.LoginController;

@Intercepts(before = AuthorizationInterceptor.class)
public class AuthenticationInterceptor implements Interceptor {

	private UsuarioLogado logado;
	private Result result;

	@Inject
	public AuthenticationInterceptor(UsuarioLogado logado, Result result) {
		this.logado = logado;
		this.result = result;
	}
	
	public AuthenticationInterceptor() {
	}

	@Override
	public boolean accepts(ControllerMethod method) {
		return !method.getController().getType().equals(LoginController.class)
				|| !method.containsAnnotation(Livre.class);
	}

	@Override
	public void intercept(InterceptorStack stack, ControllerMethod method,
			Object controller) throws InterceptionException {
		if(!method.containsAnnotation(Livre.class)) {
			if (logado.getPessoa() != null) {
				stack.next(method, controller);
			} else {
				this.result.redirectTo(LoginController.class).index();
			}
		} else {
			stack.next(method, controller);
		}
	}

}
