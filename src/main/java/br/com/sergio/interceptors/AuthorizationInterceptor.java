package br.com.sergio.interceptors;

import javax.inject.Inject;

import br.com.caelum.vraptor.InterceptionException;
import br.com.caelum.vraptor.Intercepts;
import br.com.caelum.vraptor.Result;
import br.com.caelum.vraptor.controller.ControllerMethod;
import br.com.caelum.vraptor.core.InterceptorStack;
import br.com.caelum.vraptor.interceptor.Interceptor;
import br.com.sergio.annotations.Livre;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.controllers.FalhaController;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Pessoa;

@Intercepts(after = AuthenticationInterceptor.class)
public class AuthorizationInterceptor implements Interceptor {

	private Result result;
	private UsuarioLogado logado;

	@Inject
	public AuthorizationInterceptor(UsuarioLogado logado, Result result) {
		this.logado = logado;
		this.result = result;
	}

	public AuthorizationInterceptor() {
	}

	@Override
	public boolean accepts(ControllerMethod method) {
		return !method.containsAnnotation(Livre.class);
	}

	@Override
	public void intercept(InterceptorStack stack, ControllerMethod method,
			Object controller) throws InterceptionException {
		Pessoa pessoa = logado.getPessoa();
		if (pessoa instanceof Atendente) {
			if (((Atendente) pessoa).isAdmin()) {
				stack.next(method, controller);
			} else {
				this.result.redirectTo(FalhaController.class).falha();
			}
		} else {
			stack.next(method, controller);
		}
	}
}
