package br.com.sergio.vos;

import java.math.BigDecimal;
import java.util.List;

import br.com.sergio.models.ESituacaoProduto;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Item;

public class ProdutoVO {

	private Long id;
	private Long codigo;
	private String descricao;
	private String nome;
	private BigDecimal precoCusto;
	private BigDecimal precoVenda;
	private ESituacaoProduto situacao;
	private Estabelecimento estabelecimento;
	private List<Item> itens;

	public ProdutoVO(Long id, Long codigo, String descricao, String nome,
			BigDecimal precoCusto, BigDecimal precoVenda,
			ESituacaoProduto situacao, Estabelecimento estabelecimento,
			List<Item> itens) {
		super();
		this.id = id;
		this.codigo = codigo;
		this.descricao = descricao;
		this.nome = nome;
		this.precoCusto = precoCusto;
		this.precoVenda = precoVenda;
		this.situacao = situacao;
		this.estabelecimento = estabelecimento;
		this.itens = itens;
	}

	public Long getId() {
		return id;
	}

	public Long getCodigo() {
		return codigo;
	}

	public String getDescricao() {
		return descricao;
	}

	public String getNome() {
		return nome;
	}

	public BigDecimal getPrecoCusto() {
		return precoCusto;
	}

	public BigDecimal getPrecoVenda() {
		return precoVenda;
	}

	public ESituacaoProduto getSituacao() {
		return situacao;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public List<Item> getItens() {
		return itens;
	}

	public Item toItem(ItemVO vo) {
		Item i = new Item();
		i.setId(vo.getId());
		i.setPedido(vo.getPedido());
//		i.setProduto(vo.getProduto());
		i.setQuantidade(vo.getQuantidade());
		i.setTotal(vo.getTotal());
		return i;
	}

}
