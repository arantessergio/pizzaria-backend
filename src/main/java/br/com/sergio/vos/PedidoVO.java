package br.com.sergio.vos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import br.com.sergio.models.Atendente;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.ESituacaoPedido;
import br.com.sergio.models.ETipoPedido;
import br.com.sergio.models.Entregador;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.FormaPagamento;
import br.com.sergio.models.Item;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Sabor;

public class PedidoVO {

	private Long id;
	private Cliente cliente;
	private Entregador motoboy;
	private Atendente atendente;
	private ESituacaoPedido situacao;
	private List<Item> itens;
	private String sabores;
	private Estabelecimento estabelecimento;
	private FormaPagamento formaPagamento;
	private String data;
	private String horaEntrega;
	private String horaRealizacao;
	private String horaSaida;
	private BigDecimal total;
	private Long quantidadeItens;
	private Long quantidadeProdutos;
	private String numero;
	private ETipoPedido tipoPedido;
	private String observacao;
	private String tempoPreparo;

	public PedidoVO(Pedido pedido) {
		if (pedido != null) {

			Calendar cal = Calendar.getInstance();
			cal.setTime(pedido.getHoraRealizacao());
			cal.add(Calendar.HOUR_OF_DAY, 2);
			Date horaRealizacaoPedidoHV = cal.getTime();

			SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy",
					Locale.getDefault());
			String dataPedido = dateFormat.format(pedido.getData());

			SimpleDateFormat formatHora = new SimpleDateFormat("HH:mm:ss",
					Locale.getDefault());
			String horaPedido = formatHora.format(horaRealizacaoPedidoHV);

			Date horaEntrega = pedido.getHoraEntrega();
			Date horaEntregaPedidoHV = horaEntrega;

			if (horaEntrega != null) {
				cal.setTime(horaEntrega);
				cal.add(Calendar.HOUR_OF_DAY, 2);
				horaEntregaPedidoHV = cal.getTime();
			}

			String entrega = "";
			if (horaEntrega != null) {
				entrega = formatHora.format(horaEntregaPedidoHV);
			}

			String saida = "";
			Date hrSaida = pedido.getHoraSaida();

			Date horaSaidaPedidoHV = hrSaida;

			if (hrSaida != null) {
				cal.setTime(pedido.getHoraRealizacao());
				cal.add(Calendar.HOUR_OF_DAY, 2);
				horaSaidaPedidoHV = cal.getTime();
			}

			if (horaSaidaPedidoHV != null) {
				saida = formatHora.format(horaSaidaPedidoHV);
			}

			StringBuilder builder = new StringBuilder();
			if (itens != null) {
				for (Item item : itens) {
					List<Sabor> sabs = item.getSabores();
					for (Iterator<Sabor> iterator = sabs.iterator(); iterator
							.hasNext();) {
						Sabor sabor = (Sabor) iterator.next();
						builder.append(sabor.getDescricao().toUpperCase());
						if (iterator.hasNext()) {
							builder.append(", ");
						}
					}
				}
			}

			this.atendente = pedido.getAtendente();
			this.cliente = pedido.getCliente();
			this.data = dataPedido;
			this.estabelecimento = pedido.getEstabelecimento();
			this.formaPagamento = pedido.getFormaPagamento();
			this.horaEntrega = entrega;
			this.horaRealizacao = horaPedido;
			this.horaSaida = saida;
			this.id = pedido.getId();
			this.sabores = builder.toString();
			this.itens = pedido.getItens();
			this.motoboy = pedido.getMotoboy();
			this.situacao = pedido.getSituacao();
			this.total = pedido.getTotal();
			this.quantidadeItens = Long.valueOf(pedido.getItens().size());
			this.numero = pedido.getNumero();
			this.tipoPedido = pedido.getTipoPedido();
			this.observacao = pedido.getObservacao();
			this.tempoPreparo = pedido.getTempoPreparo();
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public void setMotoboy(Entregador motoboy) {
		this.motoboy = motoboy;
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public void setAtendente(Atendente atendente) {
		this.atendente = atendente;
	}

	public ESituacaoPedido getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoPedido situacao) {
		this.situacao = situacao;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public Long getQuantidadeItens() {
		this.quantidadeItens = Long.valueOf(itens.size());
		return quantidadeItens;
	}

	public Long getQuantidadeProdutos() {
		for (Item item : itens)
			quantidadeProdutos = item.getQuantidade();
		return quantidadeProdutos;
	}

	public void setQuantidadeProdutos(Long quantidadeProdutos) {
		this.quantidadeProdutos = quantidadeProdutos;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public void setQuantidadeItens(Long quantidadeItens) {
		this.quantidadeItens = quantidadeItens;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(String horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public String getHoraRealizacao() {
		return horaRealizacao;
	}

	public void setHoraRealizacao(String horaRealizacao) {
		this.horaRealizacao = horaRealizacao;
	}

	public String getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(String horaSaida) {
		this.horaSaida = horaSaida;
	}

	public String getSabores() {
		return sabores;
	}

	public void setSabores(String sabores) {
		this.sabores = sabores;
	}

	public ETipoPedido getTipoPedido() {
		return tipoPedido;
	}

	public String getTempoPreparo() {
		return tempoPreparo;
	}

	public static Pedido toPedido(PedidoVO vo) {
		Pedido p = new Pedido();
		p.setAtendente(vo.getAtendente());
		p.setCliente(vo.getCliente());
		p.setData(p.getData());
		p.setEstabelecimento(vo.getEstabelecimento());
		p.setFormaPagamento(vo.getFormaPagamento());
		p.setId(vo.getId());
		p.setItens(vo.getItens());
		p.setMotoboy(vo.getMotoboy());
		p.setSituacao(vo.getSituacao());
		p.setTotal(vo.getTotal());
		p.setNumero(vo.getNumero());
		return p;
	}

	public String getObservacao() {
		return observacao;
	}

}
