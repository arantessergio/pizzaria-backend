package br.com.sergio.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;

@Entity
public class Item implements Serializable {

	private static final long serialVersionUID = 5693370235770425885L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "pedido_id")
	private Pedido pedido;

	private Long quantidade;

	private BigDecimal total;

	@Enumerated(EnumType.STRING)
	private ETamanhoPizza tamanhoPizza;

	@ManyToMany(fetch = FetchType.EAGER)
	@JoinTable(name = "item_sabor", joinColumns = { @JoinColumn(name = "item_id") }, inverseJoinColumns = { @JoinColumn(name = "sabor_id") })
	private List<Sabor> sabores;

	@Enumerated(EnumType.STRING)
	private ETipoBorda borda;

	private String observacao;

	public Item() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public void setPedido(Pedido pedido) {
		this.pedido = pedido;
	}

	// public Produto getProduto() {
	// return produto;
	// }
	//
	// public void setProduto(Produto produto) {
	// this.produto = produto;
	// }

	public Long getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(Long quantidade) {
		this.quantidade = quantidade;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public List<Sabor> getSabores() {
		return sabores;
	}

	public void setSabores(List<Sabor> sabores) {
		this.sabores = sabores;
	}

	// public Bebida getBebida() {
	// return bebida;
	// }
	//
	// public void setBebida(Bebida bebida) {
	// this.bebida = bebida;
	// }

	// public Integer getQuantidadeBebida() {
	// return quantidadeBebida;
	// }
	//
	// public List<Bebida> getBebidas() {
	// return bebidas;
	// }
	//
	// public void setBebidas(List<Bebida> bebidas) {
	// this.bebidas = bebidas;
	// }
	//
	// public void setQuantidadeBebida(Integer quantidadeBebida) {
	// this.quantidadeBebida = quantidadeBebida;
	// }

	public ETamanhoPizza getTamanhoPizza() {
		return tamanhoPizza;
	}

	public void setTamanhoPizza(ETamanhoPizza tamanhoPizza) {
		this.tamanhoPizza = tamanhoPizza;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public ETipoBorda getBorda() {
		return borda;
	}

	public void setBorda(ETipoBorda borda) {
		this.borda = borda;
	}

}
