package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Cidade;
import br.com.sergio.models.EUf;
import br.com.sergio.models.Estado;

public class EstadoVO {

	private Long id;
	private String nome;
	private EUf uf;
	private List<Cidade> cidades;

	public EstadoVO(Long id, String nome, EUf uf, List<Cidade> cidades) {
		super();
		this.id = id;
		this.nome = nome;
		this.uf = uf;
		this.cidades = cidades;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public EUf getUf() {
		return uf;
	}

	public List<Cidade> getCidades() {
		return cidades;
	}

	public Estado toEstado(EstadoVO vo) {
		Estado e = new Estado();
		e.setId(vo.getId());
		e.setCidades(vo.getCidades());
		e.setNome(vo.getNome());
		e.setUf(vo.getUf());
		return e;
	}

}
