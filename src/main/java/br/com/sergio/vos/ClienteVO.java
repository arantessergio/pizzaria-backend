package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Cliente;
import br.com.sergio.models.EStatus;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Telefone;

public class ClienteVO extends PessoaVO {

	private String email;
	private String observacao;
	private List<Pedido> pedidos;
	private List<Telefone> telefones;
	private Estabelecimento estabelecimento;

	public ClienteVO(Long id, String nome, String rg, String cpf,
			EStatus status, String login, String senha, Endereco endereco) {
		super(id, nome, rg, cpf, status, login, senha, endereco);
	}

	public String getEmail() {
		return email;
	}

	public String getObservacao() {
		return observacao;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public Cliente toCliente(ClienteVO vo) {
		Cliente c = new Cliente();
		c.setCpf(vo.getCpf());
		c.setEmail(vo.getEmail());
		c.setEndereco(vo.getEndereco());
		c.setEstabelecimento(vo.getEstabelecimento());
		c.setId(vo.getId());
		c.setLogin(vo.getLogin());
		c.setNome(vo.getNome());
		c.setObservacao(vo.getObservacao());
		c.setPedidos(vo.getPedidos());
		c.setRg(vo.getRg());
		c.setSenha(vo.getSenha());
		c.setStatus(vo.getStatus());
		// c.setTelefones(vo.getTelefones());
		return c;
	}

}
