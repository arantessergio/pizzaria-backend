<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria - Pedidos</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Pedido ${ numero }</h2>
				<hr>
				<form class="form-horizontal"
					action="${ linkTo[PedidosController].salvar }" method="post">
					<input id="txtId" type="hidden" name="pedido.id"
						value="${ idPedido }"> <input id="idEstabelecimento"
						type="hidden" name="pedido.estabelecimento.id"
						value="${ idEstabelecimento }">
					<fieldset>
					<c:if test="${ entrega }">
						<c:if test="${ pedidoPronto }">
							<div class="form-group">
								<div class="col-md-4">
									<select id="cmbEstado" name="pedido.motoboy.nome"
										class="form-control">
										<c:forEach items="${ entregadores }" var="entregador">
											<option value="${ entregador.id }">${ entregador.nome }</option>
										</c:forEach>
									</select>
								</div>
							</div>
						</c:if>
					</c:if>
						<br> Situação:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="situacao" name="pedido.situacao"
									class="form-control">
									<option value="REALIZADO">REALIZADO</option>
									<option value="RECEBIDO">RECEBIDO</option>
									<option value="PREPARANDO">PREPARANDO</option>
									<option value="PRONTO">PRONTO</option>
									<option value="SAIU_ENTREGA">SAIU_ENTREGA</option>
									<option value="ENTREGUE">ENTREGUE</option>
									<option value="CANCELADO">CANCELADO</option>
								</select>
							</div>
						</div>
						<c:if test="${ desconto }">
							<div class="form-group">
								Desconto:<br />
								<div class="col-md-4">
									<input class="form-control" id="descricao"
										name="pedido.desconto" placeholder="$$ Desconto"
										value="${ pedido.desconto }" />
								</div>
							</div>
						</c:if>
						<div class="form-group">
								Tempo de preparo:<br />
								<div class="col-md-4">
									<input class="form-control" id="descricao"
										name="pedido.tempoPreparo" placeholder="Mais quantos minutos?"
										value="${ pedido.tempoPreparo }" />
								</div>
							</div>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>

					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
					<c:if test="${not empty success}">
						<div class="alert alert-danger" role="alert">${ success }</div>
					</c:if>

				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>