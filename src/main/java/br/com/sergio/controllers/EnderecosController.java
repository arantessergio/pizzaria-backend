package br.com.sergio.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.models.Endereco;
import br.com.sergio.persistence.novo.EnderecoDao;

@Controller
public class EnderecosController {

	private Result result;
	private EnderecoDao enderecoDao;

	@Inject
	public EnderecosController(Result result, EnderecoDao enderecoDao) {
		super();
		this.result = result;
		this.enderecoDao = enderecoDao;
	}
	
	public EnderecosController() {
	}

	public void formulario() {
	}

	@Post("/enderecos")
	public void salvar(Endereco endereco) {
	}

	@Delete("/enderecos/{id}")
	public void remover(Long id) {
	}

	@Get("/enderecos")
	public void listar() {
	}

}
