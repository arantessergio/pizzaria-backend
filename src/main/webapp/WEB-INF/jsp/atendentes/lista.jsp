<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Atendentes</h2>

				<hr>

				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[AtendentesController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[AtendentesController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Nome</th>
							<th>RG</th>
							<th>CPF</th>
							<th>Estado</th>
							<th>Cidade</th>
							<th>Bairro</th>
							<th>Rua</th>
							<th>Número</th>
							<th>Status</th>
							<th>Login</th>
							<th>--</th>
							<th>--</th>
						</tr>
						<c:forEach items="${ atendenteList }" var="atendente">
							<tr>
								<td><c:out value="${ atendente.nome }" /></td>
								<td><c:out value="${ atendente.rg }" /></td>
								<td><c:out value="${ atendente.cpf }" /></td>
								<td><c:out value="${ atendente.endereco.estado.nome }" /></td>
								<td><c:out value="${ atendente.endereco.cidade.nome }" /></td>
								<td><c:out value="${ atendente.endereco.bairro.nome }" /></td>
								<td><c:out value="${ atendente.endereco.rua }" /></td>
								<td><c:out value="${ atendente.endereco.numero }" /></td>
								<td><c:out value="${ atendente.status }" /></td>
								<td><c:out value="${ atendente.login }" /></td>
								<td><a
									href="${ linkTo[AtendentesController].edita(atendente.id)}"
									class="btn btn-info">Editar</a></td>
								<td><a
									href="${ linkTo[AtendentesController].solicitaexclusao(atendente.id) }"
									class="btn btn-danger">Remover</a></td>
							</tr>
						</c:forEach>
					</table>
					<div class="modal-footer">
						<c:if test="${not empty erro}">
							<div class="alert alert-danger" role="alert">${ erro }</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>