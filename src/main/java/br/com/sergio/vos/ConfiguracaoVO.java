package br.com.sergio.vos;

import java.math.BigDecimal;

import br.com.sergio.models.Configuracao;

public class ConfiguracaoVO {

	private Long id;
	private String descricao;
	private Boolean aberto;
	private BigDecimal valorFrete;

	public ConfiguracaoVO(Long id, String descricao, Boolean aberto,
			BigDecimal valorFrete) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.aberto = aberto;
		this.valorFrete = valorFrete;
	}

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public Boolean getAberto() {
		return aberto;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public Configuracao toConfiguracao(ConfiguracaoVO vo) {
		Configuracao c = new Configuracao();
		// c.setAberto(vo.getAberto());
		// c.setDescricao(vo.getDescricao());
		c.setId(vo.getId());
		c.setValorFrete(vo.getValorFrete());
		return c;
	}

}
