package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Configuracao;

@Stateless
public class ConfiguracaoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public ConfiguracaoDao() {
	}

	public void salvar(Configuracao t) {
		if (t.getId() == null) {
			this.manager.persist(t);
		} else {
			this.alterar(t);
		}
	}

	public void alterar(Configuracao t) {
		this.manager.merge(t);
	}

	public void remover(Configuracao t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Configuracao> listar() {
		try {
			return this.manager.createQuery("select c from Configuracao c",
					Configuracao.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Configuracao>();
		}
	}

}
