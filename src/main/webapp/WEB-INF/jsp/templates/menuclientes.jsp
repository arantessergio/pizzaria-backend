<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-sm-3">
	<hr>
	<ul class="nav nav-stacked collapse in" id="userMenu">
		<li class="active"><a href="${ linkTo[InicioController].index }"><i
				class="glyphicon "></i> Início</a></li>
		<li><a href="${ linkTo[PedidosController].lista }"><i
				class="glyphicon "></i> Pedidos</a></li>
		<li><a href="${ linkTo[SaboresController].lista }"><i
				class="glyphicon "></i> Sabores</a></li>
		<li><a href="${ linkTo[BebidasController].lista }"><i
				class="glyphicon "></i> Bebidas</a></li>
	</ul>
</div>