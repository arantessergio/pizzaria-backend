package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Bairro;
import br.com.sergio.models.ComboModel;

@Stateless
public class BairroDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public BairroDao() {
	}

	public void salvar(Bairro t) {
		if (t.getId() == null)
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		else
			this.alterar(t);
	}

	public void alterar(Bairro t) {
		this.manager.merge(t);
	}

	public void remover(Bairro t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Bairro> listar() {
		try {
			return this.manager.createQuery("select b from Bairro b",
					Bairro.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Bairro>();
		}
	}

	public Bairro buscarPorNome(String nome, Long idCidade) {
		try {
			return this.manager
					.createQuery(
							"select b from Bairro b where b.nome = :nome and b.cidade.id = :idcidade",
							Bairro.class).setParameter("nome", nome)
					.setParameter("idcidade", idCidade).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<ComboModel> toComboModel() {
		List<Bairro> list = this.listar();
		List<ComboModel> result = new ArrayList<ComboModel>();
		for (Bairro bairro : list) {
			result.add(new ComboModel(bairro.getId(), bairro.getNome()));
		}
		return result;
	}

	public Bairro buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select b from Bairro b where b.id = :id",
							Bairro.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Bairro> buscarPorCidade(Long idCidade) {
		try {
			return this.manager
					.createQuery(
							"select b from Bairro b where b.cidade.id = :id",
							Bairro.class).setParameter("id", idCidade)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Bairro>();
		}
	}

}
