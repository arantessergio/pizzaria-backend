package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.FormaPagamento;
import br.com.sergio.models.Pedido;

public class FormaPagamentoVO {

	private Long id;
	private String descricao;
	private List<Pedido> pedidos;

	public FormaPagamentoVO(Long id, String descricao, List<Pedido> pedidos) {
		super();
		this.id = id;
		this.descricao = descricao;
		this.pedidos = pedidos;
	}

	public Long getId() {
		return id;
	}

	public String getDescricao() {
		return descricao;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public FormaPagamento toFormaPagamento(FormaPagamentoVO vo) {
		FormaPagamento f = new FormaPagamento();
		f.setDescricao(vo.getDescricao());
		f.setId(vo.getId());
		f.setPedidos(vo.getPedidos());
		return f;
	}

}
