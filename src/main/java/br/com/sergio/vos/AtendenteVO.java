package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Atendente;
import br.com.sergio.models.EStatus;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Telefone;

public class AtendenteVO extends PessoaVO {

	private boolean admin;
	private List<Pedido> pedidos;
	private List<Telefone> telefones;
	private Estabelecimento estabelecimento;

	public AtendenteVO(Long id, String nome, String rg, String cpf,
			EStatus status, String login, String senha, Endereco endereco) {
		super(id, nome, rg, cpf, status, login, senha, endereco);
	}

	public boolean isAdmin() {
		return admin;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public Atendente toAtendente(AtendenteVO vo) {
		Atendente a = new Atendente();
		a.setAdmin(vo.isAdmin());
		a.setCpf(vo.getCpf());
		a.setEndereco(vo.getEndereco());
		a.setEstabelecimento(vo.getEstabelecimento());
		a.setId(vo.getId());
		a.setLogin(vo.getLogin());
		a.setNome(vo.getNome());
		a.setPedidos(vo.getPedidos());
		a.setRg(vo.getRg());
		a.setSenha(vo.getSenha());
		a.setStatus(vo.getStatus());
		a.setTelefones(vo.getTelefones());
		return a;
	}

}
