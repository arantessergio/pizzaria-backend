package br.com.sergio.exception;

public class PrecoVendaInferiorPrecoCustoException extends RuntimeException {

	private static final long serialVersionUID = -3936293383864334561L;

	public PrecoVendaInferiorPrecoCustoException(String message) {
		super(message);
	}

}
