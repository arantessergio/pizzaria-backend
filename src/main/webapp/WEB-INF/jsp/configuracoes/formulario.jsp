<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>
	<jsp:include page="../templates/barrasuperior.jsp" />
	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Configurações</h2>
				<form class="form-horizontal"
					action="${ linkTo[ConfiguracoesController].salvar }" method="post">
					<c:if test="${ not empty idEstabelecimento }">
						<input type="hidden" value="${ idEstabelecimento }"
							name="configuracao.estabelecimento.id">
					</c:if>
					<c:if test="${ not empty configuracao }">
						<input type="hidden" name="configuracao.id"
							value="${ configuracao.id }">
					</c:if>
					<fieldset>
						<div class="form-group">
							<label class="col-md-4 control-label" for="descricao">Pequena</label>
							<div class="col-md-4">
								<input class="form-control" id="valorPequena"
									name="configuracao.valorPequena"
									value="${ configuracao.valorPequena }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="descricao">Média</label>
							<div class="col-md-4">
								<input class="form-control" id="valorMedia"
									name="configuracao.valorMedia"
									value="${ configuracao.valorMedia }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="descricao">Grande</label>
							<div class="col-md-4">
								<input class="form-control" id="valorGrande"
									name="configuracao.valorGrande"
									value="${ configuracao.valorGrande }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="descricao">Gigante</label>
							<div class="col-md-4">
								<input class="form-control" id="valorGigante"
									name="configuracao.valorGigante"
									value="${ configuracao.valorGigante }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="descricao">Gigante
								com borda</label>
							<div class="col-md-4">
								<input class="form-control" id="valorGigante"
									name="configuracao.valorGiganteBorda"
									value="${ configuracao.valorGiganteBorda }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="valorBorda">Valor
								borda:</label>
							<div class="col-md-4">
								<input class="form-control" id="valorMedia"
									name="configuracao.valorBorda"
									value="${ configuracao.valorBorda }" />
							</div>
						</div>
						<br />
						<div class="form-group">
							<label class="col-md-4 control-label" for="btnSalvar"></label>
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>

					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>