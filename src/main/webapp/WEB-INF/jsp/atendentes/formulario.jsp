<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>
	<jsp:include page="../templates/barrasuperior.jsp" />
	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Atendentes</h2>
				<hr>
				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[AtendentesController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[AtendentesController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<form class="form-horizontal"
					action="${ linkTo[AtendentesController].salvar }" method="post">
					<fieldset>
						<input type="hidden" value="${ idEstabelecimento }"
							name="atendente.estabelecimento.id"> <input type="hidden"
							name="atendente.id" value="${ atendente.id }">
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtNome" placeholder="Nome"
									class="form-control input-md" required="required" type="text"
									name="atendente.nome" value="${ atendente.nome }">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtRg" placeholder="RG" class="form-control input-md"
									required="required" type="text" name="atendente.rg"
									value="${ atendente.rg }">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtCpf" placeholder="CPF"
									class="form-control input-md" required="required" type="text"
									name="atendente.cpf" value="${ atendente.cpf }">

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4">
								<select id="cmbEstado"
									name="atendente.endereco.bairro.cidade.estado.nome"
									class="form-control">
									<c:forEach items="${ estados }" var="estado">
										<option>${ estado.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4">
								<select id="cmbCidade"
									name="atendente.endereco.bairro.cidade.nome"
									class="form-control">
									<c:forEach items="${ cidades }" var="cidade">
										<option>${ cidade.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4">
								<select id="cmbBairro" name="atendente.endereco.bairro.nome"
									class="form-control">
									<c:forEach items="${ bairros }" var="bairro">
										<option>${ bairro.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtRua" placeholder="Rua"
									class="form-control input-md" type="text"
									name="atendente.endereco.rua"
									value="${ atendente.endereco.rua }">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtNumero" placeholder="Número"
									class="form-control input-md" type="text"
									name="atendente.endereco.numero"
									value="${ atendente.endereco.numero }">
								<div class="checkbox">
									<label for="semNumero-0"> <input type="checkbox"
										name="semNumero" id="semNumero-0"
										value="${ atendente.endereco.semNumero }"> Sem número
									</label>
								</div>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4">
								<input id="txtLogin" placeholder="Login"
									class="form-control input-md" type="text"
									name="atendente.login" value="${ atendente.login }">

							</div>
						</div>

						<!-- Password input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtSenha" placeholder="Senha"
									class="form-control input-md" type="password"
									name="atendente.senha" value="${ atendente.senha }">

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-4">
								<div class="checkbox">
									<label for="semNumero-0"> <input type="checkbox"
										name="semNumero" id="semNumero-0" value="${ atendente.admin }">
										Administrador
									</label>
								</div>

							</div>
						</div>

						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>

						<c:if test="${ not empty sucesso }">
							<ul>
								<li><h3>${ sucesso }</h3></li>
							</ul>
						</c:if>

						<c:if test="${ not empty senhaInvalida }">
							<ul>
								<li><h3>${ senhaInvalida }</h3></li>
							</ul>
						</c:if>

						<c:if test="${ not empty usuarioInvalido }">
							<ul>
								<li><h3>${ usuarioInvalido }</h3></li>
							</ul>
						</c:if>

						<c:if test="${ not empty cpfInvalido }">
							<ul>
								<li><h3>${ cpfInvalido }</h3></li>
							</ul>
						</c:if>

					</fieldset>
				</form>

				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>