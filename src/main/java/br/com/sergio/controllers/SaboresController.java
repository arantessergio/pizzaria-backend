package br.com.sergio.controllers;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Transactional;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Pessoa;
import br.com.sergio.models.Sabor;
import br.com.sergio.persistence.novo.SaborDao;

@Controller
public class SaboresController {

	private Result result;
	private SaborDao saborDao;
	private UsuarioLogado logado;

	@Inject
	public SaboresController(Result result, SaborDao saborDao,
			UsuarioLogado logado) {
		this.result = result;
		this.saborDao = saborDao;
		this.logado = logado;
	}

	public SaboresController() {
	}

	public void formulario() {
		inserirInformacoesCabecalho();
	}

	@Transactional
	public void salvar(Sabor sabor) {
		if (sabor != null) {
			Estabelecimento estabelecimento = ((Atendente)this.logado.getPessoa()).getEstabelecimento();
			sabor.setEstabelecimento(estabelecimento);
			this.saborDao.salvar(sabor);
			this.result.include("success", "Sabor salvo com sucesso!");
			this.result.redirectTo(this).lista();
		}
	}

	@Path("/sabores/editar/{id}")
	public void editar(Long id) {
		Sabor sabor = this.saborDao.buscarPorId(id);
		if (sabor != null) {
			this.result.include(sabor);
			this.result.redirectTo(this).formulario();
		} else {
			this.result.notFound();
		}
	}

	@Get("/sabores")
	public List<Sabor> lista() {
		inserirInformacoesCabecalho();
		Long idEstabelecimento = ((Atendente)this.logado.getPessoa()).getEstabelecimento().getId();
		List<Sabor> sabores = saborDao.listarPorEstabelecimento(idEstabelecimento);
		Collections.sort(sabores, new Comparator<Sabor>() {
			@Override
			public int compare(Sabor o1, Sabor o2) {
				return o1.getDescricao().compareTo(o2.getDescricao());
			}
		});
		return sabores;
	}

	private void inserirInformacoesCabecalho() {
		Pessoa pessoa = logado.getPessoa();
		if (pessoa instanceof Atendente) {
			Atendente atendente = (Atendente) pessoa;
			if (atendente != null) {
				String nomeFantasia = atendente.getEstabelecimento()
						.getNomeFantasia();
				String nome = atendente.getNome();
				this.result.include("estabelecimento", nomeFantasia);
				this.result.include("nome", nome);
				this.result.include("id_estabelecimento", atendente
						.getEstabelecimento().getId());
				this.result.include("atendente", true);
			}
		} else {
			Cliente cliente = (Cliente) pessoa;
			String nome = cliente.getNome();
			this.result.include("nome", nome);
			this.result.include("cliente", true);
		}
	}

	@Path("/sabores/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Sabor sabor = this.saborDao.buscarPorId(id);
		if (sabor != null) {
			this.result.include("id", sabor.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void confirmaexclusao() {
	}

	@Path("/sabores/excluir/{id}")
	public void excluir(Long id) {
		Sabor sabor = this.saborDao.buscarPorId(id);
		if (sabor != null) {
			this.saborDao.remover(sabor);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

}
