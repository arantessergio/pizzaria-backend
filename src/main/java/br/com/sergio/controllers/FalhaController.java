package br.com.sergio.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Livre;

@Controller
public class FalhaController {

	@Inject
	private Result result;
	
	public FalhaController() {
	}

	public FalhaController(Result result) {
		this.result = result;
	}
	
	@Livre
	public void falha() {
	}
	
}
