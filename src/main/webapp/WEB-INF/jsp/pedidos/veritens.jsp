<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<c:if test="${ cliente }">
				<jsp:include page="../templates/menuclientes.jsp" />
			</c:if>
			<c:if test="${ atendente }">
				<jsp:include page="../templates/menulateral.jsp" />
			</c:if>
			<div class="col-sm-9">
				<h2>Itens</h2>
				<hr>
				<h4>Pizzas:</h4>
				<br />
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Tamanho</th>
							<th>Qtde</th>
							<th>Sabores</th>
							<th>Borda</th>
							<th>Total</th>
						</tr>
						<c:forEach items="${ itemPizzas }" var="item">
							<tr>
								<td><c:out value="${ item.tamanhoPizza }" /></td>
								<td><c:out value="${ item.quantidade }" /></td>
								<td><c:forEach items="${ item.sabores }" var="sabor">
										[ <c:out value="${ sabor.descricao }" /> ]
									</c:forEach></td>
								<td><c:out value="${ item.borda }" /></td>
								<td><c:out value="${ item.total }" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<br />
				<h4>Bebidas:</h4>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Bebida</th>
							<th>Quantidade</th>
							<th>Observação</th>
							<th>Total</th>
						</tr>
						<c:forEach items="${ itemBebidas }" var="bebida">
							<tr>
								<td><c:forEach items="${ bebida.bebidas }" var="bebs">
												[ <c:out value="${ bebs.descricao }" /> ]
											</c:forEach></td>
								<td><c:out value="${ bebida.quantidade }" /></td>
								<td><c:out value="${ bebida.observacao}"></c:out></td>
								<td><c:out value="${ bebida.total }" /></td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<c:if test="${not empty mensagem}">
					<div class="alert alert-success" role="alert">${ mensagem }</div>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>