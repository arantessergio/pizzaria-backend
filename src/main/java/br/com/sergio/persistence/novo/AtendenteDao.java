package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Atendente;

@Stateless
public class AtendenteDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public AtendenteDao() {
	}

	public void salvar(Atendente t) {
		if (t.getId() == null) {
			this.manager.persist(t);
		} else {
			this.alterar(t);
		}
	}

	public void alterar(Atendente t) {
		this.manager.merge(t);
	}

	public void remover(Atendente t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Atendente> listar() {
		try {
			return this.manager.createQuery("select a from Atendente a",
					Atendente.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Atendente>();
		}
	}

	public Atendente buscarUsuarioLoginESenha(String login, String senha) {
		try {
			return this.manager
					.createQuery(
							"select a from Atendente a where login = :login and senha = :senha",
							Atendente.class).setParameter("login", login)
					.setParameter("senha", senha).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Atendente buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select a from Atendente a where a.id = :id",
							Atendente.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Atendente buscarPorEmail(String email) {
		try {
			return this.manager
					.createQuery(
							"select a from Atendente a where a.email = :email",
							Atendente.class).setParameter("email", email)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Atendente> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select a from Atendente a where a.estabelecimento.id = :id",
							Atendente.class)
					.setParameter("id", idEstabelecimento).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Atendente>();
		}
	}

}
