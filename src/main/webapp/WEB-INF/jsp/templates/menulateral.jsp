<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div class="col-sm-3">
	<hr>
	<ul class="nav nav-stacked collapse in" id="userMenu">
		<li class="active"><a href="${ linkTo[InicioController].index }"><i
				class="glyphicon "></i> Início</a></li>
		<li><a href="${ linkTo[PedidosController].lista }"><i
				class="glyphicon "></i> Pedidos</a></li>
		<%-- 		<li><a href="${ linkTo[ProdutosController].lista }"><i --%>
		<!-- 				class="glyphicon "></i> Produtos</a></li> -->
		<li><a href="${ linkTo[SaboresController].lista }"><i
				class="glyphicon "></i> Sabores</a></li>
		<li><a href="${ linkTo[BebidasController].lista }"><i
				class="glyphicon "></i> Bebidas</a></li>
		<li><a href="${ linkTo[AtendentesController].lista }"><i
				class="glyphicon "></i> Atendentes</a></li>
		<li><a href="${ linkTo[ClientesController].lista }"><i
				class="glyphicon "></i> Clientes</a></li>
		<li><a href="${ linkTo[EntregadoresController].lista }"><i
				class="glyphicon "></i> Entregadores</a></li>
		<li><a href="${ linkTo[ConfiguracoesController].formulario }"><i
				class="glyphicon "></i> Configurações</a></li>
		<li><a href="${ linkTo[FretesController].lista }"><i
				class="glyphicon "></i> Fretes</a></li>
	</ul>
</div>