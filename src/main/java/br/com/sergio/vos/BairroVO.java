package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Endereco;

public class BairroVO {

	private Long id;
	private String nome;
	private Cidade cidade;
	private List<Endereco> enderecos;

	public BairroVO(Long id, String nome, Cidade cidade,
			List<Endereco> enderecos) {
		super();
		this.id = id;
		this.nome = nome;
		this.cidade = cidade;
		this.enderecos = enderecos;
	}

	public Long getId() {
		return id;
	}

	public String getNome() {
		return nome;
	}

	public Cidade getCidade() {
		return cidade;
	}

	public List<Endereco> getEnderecos() {
		return enderecos;
	}

	public Bairro toBairro(BairroVO vo) {
		Bairro b = new Bairro();
		b.setCidade(vo.getCidade());
		b.setEnderecos(vo.getEnderecos());
		b.setId(vo.getId());
		b.setNome(vo.getNome());
		return b;
	}

}
