<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div>
				<h2>Entregadores</h2>
				<hr>
				<div class="table-responsive">
					<div>
						<a id="btnNovo" class="btn btn-default"
							href="${ linkTo[EntregadoresController].formulario }">Novo</a> <a
							id="btnVisualizar"
							href="${ linkTo[EntregadoresController].lista }"
							class="btn btn-info">Visualizar</a>
					</div>
					<hr>
					<form class="form-horizontal"
						action="${ linkTo[EntregadoresController].salvar }" method="post">
						<fieldset>
							<input type="hidden" name="entregador.id"
								value="${ entregador.id }">
							<div class="form-group">
								<div class="col-md-6">
									<input id="txtNome" placeholder="Nome"
										class="form-control input-md" required="required" type="text"
										name="entregador.nome" value="${ entregador.nome }">

								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtRg" placeholder="RG"
										class="form-control input-md" required="required" type="text"
										name="entregador.rg" value="${ entregador.rg }">

								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtCpf" placeholder="CPF"
										class="form-control input-md" required="required" type="text"
										name="entregador.cpf" value="${ entregador.cpf }">

								</div>
							</div>

							<!-- Select Basic -->
							<div class="form-group">
								<div class="col-md-4">
									<select id="cmbEstado" name="entregador.endereco.estado.nome"
										class="form-control">
										<c:forEach items="${ estados }" var="estado">
											<option>${ estado.nome }</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<!-- Select Basic -->
							<div class="form-group">
								<div class="col-md-4">
									<select id="cmbCidade" name="entregador.endereco.cidade.nome"
										class="form-control">
										<c:forEach items="${ cidades }" var="cidade">
											<option>${ cidade.nome }</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<!-- Select Basic -->
							<div class="form-group">
								<div class="col-md-4">
									<select id="cmbBairro" name="entregador.endereco.bairro.nome"
										class="form-control">
										<c:forEach items="${ bairros }" var="bairro">
											<option>${ bairro.nome }</option>
										</c:forEach>
									</select>
								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtRua" placeholder="Rua"
										class="form-control input-md" type="text"
										name="entregador.endereco.rua"
										value="${ entregador.endereco.rua }">

								</div>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtNumero" placeholder="Número"
										class="form-control input-md" type="text"
										name="entregador.endereco.numero"
										value="${ entregador.endereco.numero }">

								</div>
							</div>
							<div>
								<a id="btnNovo" class="btn btn-default"
									href="${ linkTo[TelefonesController].entregador }">Inserir
									telefones</a>
							</div>

							<!-- Text input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtLogin" placeholder="Login"
										class="form-control input-md" type="text"
										name="entregador.login" value="${ entregador.login }">

								</div>
							</div>

							<!-- Password input-->
							<div class="form-group">
								<div class="col-md-4">
									<input id="txtSenha" placeholder="Senha"
										class="form-control input-md" type="password"
										name="entregador.senha" value="${ entregador.senha }">

								</div>
							</div>

							<!-- Button (Double) -->
							<div class="form-group">
								<div class="col-md-8">
									<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
										type="submit">Salvar</button>
								</div>
							</div>

						</fieldset>
					</form>

					<div class="modal-footer">
						<c:if test="${not empty erro}">
							<div class="alert alert-danger" role="alert">${ erro }</div>
						</c:if>
					</div>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>