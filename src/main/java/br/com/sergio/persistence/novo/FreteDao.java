package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Frete;

@Stateless
public class FreteDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public FreteDao() {
	}

	public void salvar(Frete t) {
		if (t.getId() == null) {
			this.manager.persist(t);
		} else {
			this.alterar(t);
		}
	}

	public void alterar(Frete t) {
		this.manager.merge(t);
	}

	public void remover(Frete t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Frete> listar() {
		try {
			return this.manager.createQuery("select a from Frete a",
					Frete.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Frete>();
		}
	}

	public Frete buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select f from Frete f where f.id = :id",
							Frete.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Frete buscarPorBairro(Long id) {
		try {
			return this.manager
					.createQuery(
							"select f from Frete f where f.bairro.id = :id",
							Frete.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Frete> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select f from Frete f where f.estabelecimento.id = :id",
							Frete.class).setParameter("id", idEstabelecimento)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Frete>();
		}
	}

}
