package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Entregador;

@Stateless
public class EntregadorDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public EntregadorDao() {
	}

	public void salvar(Entregador t) {
		if (t.getId() == null)
			this.manager.persist(t);
		else
			this.alterar(t);
	}

	public void alterar(Entregador t) {
		this.manager.merge(t);
	}

	public void remover(Entregador t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Entregador> listar() {
		try {
			return this.manager.createQuery("select m from Entregador m",
					Entregador.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Entregador>();
		}
	}

	public Entregador buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select m from Entregador m where id = :id",
							Entregador.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Entregador buscarPorNome(String nome) {
		try {
			return this.manager
					.createQuery(
							"select e from Entregador e where e.nome = :nome",
							Entregador.class).setParameter("nome", nome)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Entregador> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select e from Entregador e where e.estabelecimento.id = :id",
							Entregador.class)
					.setParameter("id", idEstabelecimento).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Entregador>();
		}
	}

}
