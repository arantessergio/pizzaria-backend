<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Fretes</h2>
				<hr>

				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[FretesController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[FretesController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<form class="form-horizontal"
					action="${ linkTo[FretesController].salvar }" method="post">
					<fieldset>
						<input type="hidden" value="${ idEstabelecimento }"
							name="bebida.estabelecimento.id"> <input type="hidden"
							name="bebida.id" value="${ frete.id }"> Bairro:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="bairro" name="bairro" class="form-control">
									<c:forEach items="${ bairros }" var="bairro">
										<option value="${ bairro.id }">${ bairro.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-md-4">
								<input id="preco" placeholder="Valor"
									class="form-control input-md" required="required" name="valor"
									value="${ valor }">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>
					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>