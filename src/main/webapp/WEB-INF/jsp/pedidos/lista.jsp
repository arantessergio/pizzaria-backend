<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<c:if test="${ atendente }">
				<jsp:include page="../templates/menulateral.jsp" />
			</c:if>
			<c:if test="${ cliente }">
				<jsp:include page="../templates/menuclientes.jsp" />
			</c:if>
			<div class="col-sm-9">
				<h2>Pedidos</h2>
				<hr>
				<c:if test="${not empty success}">
					<div class="alert alert-success" role="alert">${ success }</div>
				</c:if>
				<c:if test="${ cliente }">
					<div>
						<a id="btnNovo" class="btn btn-default"
							href="${ linkTo[PedidosController].novo }">Novo</a> <a
							id="btnVisualizar" href="${ linkTo[PedidosController].lista }"
							class="btn btn-info">Visualizar</a>
					</div>
					<hr>
				</c:if>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<c:if test="${ atendente }">
								<th>Cliente</th>
								<th></th>
								<th>Motoboy</th>
							</c:if>
							<th>Atendente</th>
							<th>Situação</th>
							<th>Tempo preparo</th>
							<th>Tipo pedido</th>
							<th>Forma pagamento</th>
							<th>Data</th>
							<th>Hora entrega</th>
							<th>Hora realização</th>
							<th>Hora saída</th>
							<th>Qtde. Itens</th>
							<c:if test="${ atendente }">
								<th></th>
							</c:if>
							<th>Total</th>
							<th>Observação</th>
							<th></th>
							<c:if test="${ atendente }">
								<th></th>
							</c:if>
						</tr>
						<c:forEach items="${ pedidoVOList }" var="pedido">
							<tr>
								<c:if test="${ atendente }">
									<td><c:out value="${ pedido.cliente.nome }" /></td>
									<td><a
										href="${ linkTo[ClientesController].endereco(pedido.cliente.id) }"
										class="btn btn-info">Ver endereço</a></td>
									<td><c:out value="${ pedido.motoboy.nome }" /></td>
								</c:if>
								<td><c:out value="${ pedido.atendente.nome }" /></td>
								<td><c:out value="${ pedido.situacao }" /></td>
								<td><c:out value="${ pedido.tempoPreparo }" /></td>
								<td><c:out value="${ pedido.tipoPedido }" /></td>
								<td><c:out value="${ pedido.formaPagamento.descricao }" /></td>
								<td><c:out value="${ pedido.data }" /></td>
								<td><c:out value="${ pedido.horaEntrega }" /></td>
								<td><c:out value="${ pedido.horaRealizacao }" /></td>
								<td><c:out value="${ pedido.horaSaida }" /></td>
								<td><c:out value="${ pedido.quantidadeItens }" /></td>
								<c:if test="${ atendente }">
									<c:choose>
										<c:when
											test="${pedido.situacao eq 'CANCELADO' or pedido.situacao eq 'ENTREGUE'}">
											<td><a id="itens" class="btn btn-info disabled"
												type="button"
												href="${ linkTo[PedidosController].edita(pedido.id) }">Editar</a>
											</td>
										</c:when>
										<c:when
											test="${pedido.situacao ne 'CANCELADO' or pedido.situacao eq 'ENTREGUE'}">
											<td><a id="itens" class="btn btn-info" type="button"
												href="${ linkTo[PedidosController].edita(pedido.id) }">Editar</a>
											</td>
										</c:when>
									</c:choose>
								</c:if>
								<td><c:out value="${ pedido.total }" /></td>
								<td><c:out value="${ pedido.observacao }" /></td>
								<td><a
									href="${ linkTo[PedidosController].veritens(pedido.id) }"
									class="btn btn-info">Itens</a></td>
								<c:if test="${ atendente }">
									<td><a
										href="${ linkTo[PedidosController].imprimir(pedido.id) }"
										class="btn btn-success">Imprimir</a></td>
								</c:if>
							</tr>
						</c:forEach>
					</table>
				</div>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">${ erro }</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>