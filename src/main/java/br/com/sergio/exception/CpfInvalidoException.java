package br.com.sergio.exception;

public class CpfInvalidoException extends RuntimeException {

	private static final long serialVersionUID = -380669032575770424L;

	public CpfInvalidoException(String message) {
		super(message);
	}

}
