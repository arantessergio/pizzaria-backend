package br.com.sergio.models;

public enum ETipoBorda {

	SEM_BORDA_RECHEADA,

	CATUPIRY,

	CHEDDAR,

	BORDA_DOCE;

}
