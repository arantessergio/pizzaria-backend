package br.com.sergio.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Estabelecimento implements Serializable {

	private static final long serialVersionUID = -2903237136424017708L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private String razaoSocial;
	private String nomeFantasia;
	private String cnpj;
	private String ie;
	@OneToOne
	private Endereco endereco;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Produto.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Produto> produtos;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Pedido.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Pedido> pedidos;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Telefone.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Telefone> telefones;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Cliente.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Cliente> clientes;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Atendente.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Atendente> atendentes;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Entregador.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Entregador> motoboys;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Bebida.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Bebida> bebidas;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Sabor.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Sabor> sabores;
	@OneToMany(mappedBy = "estabelecimento", targetEntity = Frete.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<Frete> fretes;

	public Estabelecimento() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getRazaoSocial() {
		return razaoSocial;
	}

	public void setRazaoSocial(String razaoSocial) {
		this.razaoSocial = razaoSocial;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getIe() {
		return ie;
	}

	public void setIe(String ie) {
		this.ie = ie;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public List<Produto> getProdutos() {
		return produtos;
	}

	public void setProdutos(List<Produto> produtos) {
		this.produtos = produtos;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public void setPedidos(List<Pedido> pedidos) {
		this.pedidos = pedidos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public void setTelefones(List<Telefone> telefones) {
		this.telefones = telefones;
	}

	public List<Cliente> getClientes() {
		return clientes;
	}

	public void setClientes(List<Cliente> clientes) {
		this.clientes = clientes;
	}

	public List<Atendente> getAtendentes() {
		return atendentes;
	}

	public void setAtendentes(List<Atendente> atendentes) {
		this.atendentes = atendentes;
	}

	public List<Entregador> getMotoboys() {
		return motoboys;
	}

	public void setMotoboys(List<Entregador> motoboys) {
		this.motoboys = motoboys;
	}

	public List<Bebida> getBebidas() {
		return bebidas;
	}

	public void setBebidas(List<Bebida> bebidas) {
		this.bebidas = bebidas;
	}

	public List<Sabor> getSabores() {
		return sabores;
	}

	public void setSabores(List<Sabor> sabores) {
		this.sabores = sabores;
	}

	public List<Frete> getFretes() {
		return fretes;
	}

	public void setFretes(List<Frete> fretes) {
		this.fretes = fretes;
	}

}
