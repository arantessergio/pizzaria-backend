package br.com.sergio.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Entregador extends Pessoa implements Serializable {

    private static final long serialVersionUID = -4804117261395299914L;
    @OneToMany(mappedBy = "motoboy", targetEntity = Localizacao.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Localizacao> localizacoes;
    @OneToMany(mappedBy = "motoboy", targetEntity = Pedido.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Pedido> pedidos;
    @OneToMany(mappedBy = "motoboy", targetEntity = Telefone.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Telefone> telefones;
    @ManyToOne
    @JoinColumn(name = "estabelecimento_id")
    private Estabelecimento estabelecimento;

    public Entregador() {
    }

    public List<Localizacao> getLocalizacoes() {
	return localizacoes;
    }

    public void setLocalizacoes(List<Localizacao> localizacoes) {
	this.localizacoes = localizacoes;
    }

    public List<Pedido> getPedidos() {
	return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
	this.pedidos = pedidos;
    }

    public List<Telefone> getTelefones() {
	return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
	this.telefones = telefones;
    }

    public Estabelecimento getEstabelecimento() {
	return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
	this.estabelecimento = estabelecimento;
    }

}
