package br.com.sergio.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Endereco;
import br.com.sergio.persistence.novo.ClienteDao;

@Controller
public class ClientesController {

	private ClienteDao clienteDao;
	private Result result;
	private UsuarioLogado logado;

	@Inject
	public ClientesController(ClienteDao clienteDao, Result result,
			UsuarioLogado logado) {
		this.clienteDao = clienteDao;
		this.result = result;
		this.logado = logado;
	}

	public ClientesController() {
	}

	@Get
	@Path(value = "/clientes")
	public List<Cliente> lista() {
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			String nomeEstabelecimento = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nomeAt = atendenteLogado.getNome();
			this.result.include("nome", nomeAt);
			this.result.include("estabelecimento", nomeEstabelecimento);
			List<Cliente> lista = this.clienteDao.listar();
			return lista;
		}
		return new ArrayList<Cliente>();
	}

	public void listapedidos(Long idCliente) {
		this.result.redirectTo(PedidosController.class).listarPedidosCliente(
				idCliente);
	}

	@Path("/clientes/endereco/{id}")
	public void endereco(Long id) {
		Cliente cliente = this.clienteDao.buscarPorId(id);
		Endereco endereco = cliente.getEndereco();
		this.result.include("cliente", cliente.getNome());
		this.result.include("bairro", endereco.getBairro().getNome());
		this.result.include("estado", endereco.getEstado().getNome());
		this.result.include("cidade", endereco.getCidade().getNome());
		this.result.include("numero", endereco.getNumero());
		this.result.include("rua", endereco.getRua());
		this.result.include("complemento", endereco.getComplemento());
	}

}
