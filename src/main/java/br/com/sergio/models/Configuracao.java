package br.com.sergio.models;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Configuracao {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private BigDecimal valorFrete;

	private BigDecimal valorPequena;
	private BigDecimal valorMedia;
	private BigDecimal valorGrande;
	private BigDecimal valorGigante;
	private BigDecimal valorGiganteBorda;

	private BigDecimal valorBorda;

	@OneToOne
	private Estabelecimento estabelecimento;

	public Configuracao() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public BigDecimal getValorFrete() {
		return valorFrete;
	}

	public void setValorFrete(BigDecimal valorFrete) {
		this.valorFrete = valorFrete;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public BigDecimal getValorPequena() {
		return valorPequena;
	}

	public void setValorPequena(BigDecimal valorPequena) {
		this.valorPequena = valorPequena;
	}

	public BigDecimal getValorMedia() {
		return valorMedia;
	}

	public void setValorMedia(BigDecimal valorMedia) {
		this.valorMedia = valorMedia;
	}

	public BigDecimal getValorGrande() {
		return valorGrande;
	}

	public void setValorGrande(BigDecimal valorGrande) {
		this.valorGrande = valorGrande;
	}

	public BigDecimal getValorGigante() {
		return valorGigante;
	}

	public void setValorGigante(BigDecimal valorGigante) {
		this.valorGigante = valorGigante;
	}

	public BigDecimal getValorBorda() {
		return valorBorda;
	}

	public void setValorBorda(BigDecimal valorBorda) {
		this.valorBorda = valorBorda;
	}

	public BigDecimal getValorGiganteBorda() {
		return valorGiganteBorda;
	}

	public void setValorGiganteBorda(BigDecimal valorGiganteBorda) {
		this.valorGiganteBorda = valorGiganteBorda;
	}

}
