package br.com.sergio.controllers;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Livre;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Estado;
import br.com.sergio.models.Pessoa;
import br.com.sergio.persistence.novo.AtendenteDao;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.CidadeDao;
import br.com.sergio.persistence.novo.ClienteDao;
import br.com.sergio.persistence.novo.EnderecoDao;
import br.com.sergio.persistence.novo.EstadoDao;

@Controller
public class LoginController {

	private Result result;
	private AtendenteDao atendenteDao;
	private ClienteDao clienteDao;
	private UsuarioLogado logado;
	private EstadoDao estadoDao;
	private CidadeDao cidadeDao;
	private BairroDao bairroDao;
	private EnderecoDao enderecoDao;

	@Inject
	public LoginController(Result result, AtendenteDao atendenteDao,
			UsuarioLogado logado, ClienteDao clienteDao, EstadoDao estadoDao,
			CidadeDao cidadeDao, BairroDao bairroDao, EnderecoDao enderecoDao) {
		this.result = result;
		this.atendenteDao = atendenteDao;
		this.logado = logado;
		this.clienteDao = clienteDao;
		this.estadoDao = estadoDao;
		this.cidadeDao = cidadeDao;
		this.bairroDao = bairroDao;
		this.enderecoDao = enderecoDao;
	}

	public LoginController() {
	}

	@Livre
	@Path("/login")
	public void index() {
	}

	@Livre
	public void logar(Pessoa usuario) {

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");

			BigInteger hash = new BigInteger(1, md.digest(usuario.getSenha()
					.getBytes()));

			String senhaComparar = String.format("%32x", hash);

			Atendente atendente = atendenteDao.buscarPorEmail(usuario
					.getEmail());
			Cliente cliente = clienteDao.buscarPorEmail(usuario.getEmail());

			if (atendente != null && atendente.getSenha().equals(senhaComparar)) {
				logado.loga(atendente);
				this.result.include("nome", atendente.getNome());
				this.result.include("atendente", true);
				this.result.include("estabelecimento", atendente
						.getEstabelecimento().getNomeFantasia());
				this.result.redirectTo(InicioController.class).index();
			} else if (cliente != null
					&& cliente.getSenha().equals(senhaComparar)) {
				logado.loga(cliente);
				this.result.include("nome", cliente.getNome());
				this.result.include("cliente", true);
				this.result.redirectTo(InicioController.class).index();
			} else {
				this.result.include("erro",
						"Erro, verifique seu usuário ou senha!");
				this.result.redirectTo(this).index();
			}
		} catch (Exception e) {
			this.result.redirectTo(ErroController.class).paginaerro(
					e.getMessage());
		}

	}

	@Livre
	public void sair() {
		this.logado.desloga();
		this.result.redirectTo(this).index();
	}

	@Livre
	public void signup() {
		this.result.include("estados", this.estadoDao.listar());
		this.result.include("cidades", this.cidadeDao.listar());
		List<Bairro> bairros = this.bairroDao.listar();
		Collections.sort(bairros, new Comparator<Bairro>() {
			@Override
			public int compare(Bairro o1, Bairro o2) {
				return o1.getNome().compareTo(o2.getNome());
			}
		});
		this.result.include("bairros", bairros);
	}

	@Livre
	public void cadastrar(String nome, String telefone, String email,
			String estado, String cidade, String bairro, String senha,
			String rua, String numero, String complemento) {

		try {
			validarCampos(new String[] { nome, telefone, email, senha, rua,
					numero });
			validarUsuarioExistente(email);
			
			MessageDigest md = MessageDigest.getInstance("MD5");
			BigInteger hash = new BigInteger(1, md.digest(senha.getBytes()));
			String senhaGravar = String.format("%32x", hash);

			Cliente cliente = new Cliente();
			cliente.setNome(nome);
			cliente.setEmail(email);

			Estado e = this.estadoDao.buscarPorId(Long.valueOf(estado));
			Cidade c = this.cidadeDao.buscarPorId(Long.valueOf(cidade));
			Bairro b = this.bairroDao.buscarPorId(Long.valueOf(bairro));

			Endereco endereco = new Endereco();
			endereco.setBairro(b);
			endereco.setCidade(c);
			endereco.setEstado(e);
			endereco.setRua(rua);
			endereco.setComplemento(complemento);
			endereco.setNumero(Long.valueOf(numero));

			this.enderecoDao.salvar(endereco);

			cliente.setEndereco(endereco);
			cliente.setTelefone(telefone);
			// cliente.setLogin(login);
			cliente.setSenha(senhaGravar);

			this.clienteDao.salvar(cliente);
			this.logado.loga(cliente);
			this.result.redirectTo(InicioController.class).index();
		} catch (RuntimeException e) {
			this.result.include("erro", e.getMessage());
			this.result.redirectTo(this).signup();
		} catch (NoSuchAlgorithmException e1) {
			e1.printStackTrace();
		}
	}

	@Livre
	private void validarUsuarioExistente(String email) {
		List<Cliente> listaClientes = this.clienteDao.listar();
		for (Iterator<Cliente> iterator = listaClientes.iterator(); iterator
				.hasNext();) {
			Cliente cliente = (Cliente) iterator.next();
			if (email.equals(cliente.getEmail())) {
				throw new RuntimeException(
						"Já existe um cadastro com este mesmo login, tente outro por favor :)");
			}
		}
	}

	@Livre
	private void validarCampos(String[] campos) {
		for (String s : campos) {
			if (s == null || s.trim().isEmpty()) {
				throw new RuntimeException(
						"Por favor, preencha todos os campos, é rápido! :)");
			}
		}
	}

}
