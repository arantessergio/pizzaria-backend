package br.com.sergio.vos;

import br.com.sergio.models.Atendente;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.ETipoTelefone;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Entregador;
import br.com.sergio.models.Telefone;

public class TelefoneVO {

	private Long id;
	private ETipoTelefone tipo;
	private Long ddd;
	private Long numero;
	private Estabelecimento estabelecimento;
	private Cliente cliente;
	private Entregador motoboy;
	private Atendente atendente;

	public TelefoneVO(Long id, ETipoTelefone tipo, Long ddd, Long numero,
			Estabelecimento estabelecimento, Cliente cliente,
			Entregador motoboy, Atendente atendente) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.ddd = ddd;
		this.numero = numero;
		this.estabelecimento = estabelecimento;
		this.cliente = cliente;
		this.motoboy = motoboy;
		this.atendente = atendente;
	}

	public Long getId() {
		return id;
	}

	public ETipoTelefone getTipo() {
		return tipo;
	}

	public Long getDdd() {
		return ddd;
	}

	public Long getNumero() {
		return numero;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public Telefone toTelefone(TelefoneVO vo) {
		Telefone t = new Telefone();
		t.setAtendente(vo.getAtendente());
		// t.setCliente(vo.getCliente());
		t.setDdd(vo.getDdd());
		t.setEstabelecimento(vo.getEstabelecimento());
		t.setId(vo.getId());
		t.setMotoboy(vo.getMotoboy());
		t.setNumero(vo.getNumero());
		t.setTipo(vo.getTipo());
		return t;
	}

}
