package br.com.sergio.models;

public enum ETipoTelefone {

    CELULAR, COMERCIAL, FIXO, RECADO;

}
