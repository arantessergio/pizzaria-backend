package br.com.sergio.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Livre;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Estado;
import br.com.sergio.models.Frete;
import br.com.sergio.models.Pessoa;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.CidadeDao;
import br.com.sergio.persistence.novo.ConfiguracaoDao;
import br.com.sergio.persistence.novo.EstadoDao;
import br.com.sergio.persistence.novo.FreteDao;

@Controller
public class InicioController {

	private Result result;
	private UsuarioLogado logado;
	private EstadoDao estadoDao;
	private CidadeDao cidadeDao;
	private BairroDao bairroDao;
	private FreteDao freteDao;
	private ConfiguracaoDao configuracaoDao;

	@Inject
	public InicioController(Result result, UsuarioLogado logado,
			EstadoDao estadoDao, CidadeDao cidadeDao, BairroDao bairroDao,
			FreteDao freteDao, ConfiguracaoDao configuracaoDao) {
		this.result = result;
		this.logado = logado;
		this.estadoDao = estadoDao;
		this.cidadeDao = cidadeDao;
		this.bairroDao = bairroDao;
		this.freteDao = freteDao;
		this.configuracaoDao = configuracaoDao;
	}

	public InicioController() {
	}

	@Livre
	@Path("/home")
	public void index() {
		Pessoa pessoa = logado.getPessoa();
		if (pessoa != null) {
			if (pessoa instanceof Atendente) {
				this.result.include("nome", pessoa.getNome());
				this.result.include("estabelecimento", ((Atendente) pessoa)
						.getEstabelecimento().getNomeFantasia());
				this.result.include("atendente", true);
			} else {
				this.result.include("nome", pessoa.getNome());
				this.result.include("cliente", true);
				Long idBairro = pessoa.getEndereco().getBairro().getId();
				Frete frete = this.freteDao.buscarPorBairro(idBairro);
				if (frete != null) {
					this.result.include("frete", frete.getValor());
				}
				this.result.include("configs", configuracaoDao.listar());
			}
		}
	}

	public List<Estado> getListEstado() {
		return estadoDao.listar();
	}

	public List<Cidade> getListCidade() {
		return cidadeDao.listar();
	}

	public List<Bairro> getListBairro() {
		return bairroDao.listar();
	}

	public Estabelecimento getEstabelecimento() {
		Pessoa pessoa = logado.getPessoa();
		if (pessoa instanceof Atendente) {
			return ((Atendente) pessoa).getEstabelecimento();
		} else {
			return ((Cliente) pessoa).getEstabelecimento();
		}
	}

}
