package br.com.sergio.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Transactional;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.exception.PrecoVendaInferiorPrecoCustoException;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Produto;
import br.com.sergio.persistence.novo.EstabelecimentoDao;
import br.com.sergio.persistence.novo.ProdutoDao;

@Controller
public class ProdutosController {

	private Result result;
	private ProdutoDao produtoDao;
	private EstabelecimentoDao estabelecimentoDao;
	private UsuarioLogado logado;

	@Inject
	public ProdutosController(Result result, ProdutoDao produtoDao,
			EstabelecimentoDao estabelecimentoDao, UsuarioLogado logado) {
		this.result = result;
		this.produtoDao = produtoDao;
		this.estabelecimentoDao = estabelecimentoDao;
		this.logado = logado;
	}

	public ProdutosController() {
	}

	public void formulario() {
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendenteLogado.getNome();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
			this.result.include("id_estabelecimento", atendenteLogado
					.getEstabelecimento().getId());
		}
	}

	public List<Produto> lista() {
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendenteLogado.getNome();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
			return this.produtoDao.listar();
		}
		return new ArrayList<Produto>();
	}

	@Transactional
	public void salvar(Produto produto) {
		try {
			validarPrecoVenda(produto.getPrecoVenda(), produto.getPrecoCusto());
			Long codigo = validarCodigo(produto.getCodigo());
			if (codigo.compareTo(produto.getCodigo()) != 0) {
				this.result
						.include("codigoIgual",
								"O código informado já existe, foi somado 1 ao código informado, verifique!");
			}
			Estabelecimento estabelecimento = this.estabelecimentoDao
					.buscarPorId(produto.getEstabelecimento().getId());
			if (estabelecimento != null) {
				produto.setCodigo(codigo);
				produto.setEstabelecimento(estabelecimento);
				this.produtoDao.salvar(produto);
				this.result.include("success", "Produto salvo com sucesso!");
				this.result.redirectTo(this).lista();
			}
		} catch (PrecoVendaInferiorPrecoCustoException pE) {
			this.result.include("precoMinimo", pE.getMessage());
			this.result.redirectTo(this).lista();
		} catch (RuntimeException e) {
			this.result.redirectTo(ErroController.class).paginaerro(
					e.getMessage());
		}
	}

	private Long validarCodigo(Long codigo) {
		List<Produto> produtos = this.produtoDao.listar();
		for (Produto p : produtos) {
			if (p.getCodigo().compareTo(codigo) == 0) {
				codigo = Long.sum(codigo, Long.valueOf(1));
			}
		}
		return codigo;
	}

	private void validarPrecoVenda(BigDecimal precoVenda, BigDecimal precoCusto) {
		if (precoCusto.compareTo(precoVenda) > 0) {
			throw new PrecoVendaInferiorPrecoCustoException(
					"O preço de custo não pode ser menor que o preço de venda, verifique!");
		}
	}

	@Get
	@Path(value = "/produtos/{id}", priority = Path.LOWEST)
	public void editar(Long id) {
		Produto produto = this.produtoDao.buscarPorId(id);
		if (produto != null) {
			this.result.include(produto);
			this.result.of(this).formulario();
		} else {
			this.result.notFound();
		}
	}

	@Path("/produtos/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Produto produto = this.produtoDao.buscarPorId(id);
		if (produto != null) {
			this.result.include("id", produto.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void confirmaexclusao() {
	}

	@Path("/produtos/excluir/{id}")
	public void excluir(Long id) {
		Produto produto = this.produtoDao.buscarPorId(id);
		if (produto != null) {
			this.produtoDao.remover(produto);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

}
