package br.com.sergio.vos;

import br.com.sergio.models.Endereco;
import br.com.sergio.models.Localizacao;
import br.com.sergio.models.Entregador;

public class LocalizacaoVO {

	private Long id;
	private Double latitude;
	private Double longitude;
	private Endereco endereco;
	private Entregador motoboy;

	public LocalizacaoVO(Long id, Double latitude, Double longitude,
			Endereco endereco, Entregador motoboy) {
		super();
		this.id = id;
		this.latitude = latitude;
		this.longitude = longitude;
		this.endereco = endereco;
		this.motoboy = motoboy;
	}

	public Long getId() {
		return id;
	}

	public Double getLatitude() {
		return latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public Localizacao toLocalizacao(LocalizacaoVO vo) {
		Localizacao l = new Localizacao();
		l.setEndereco(vo.getEndereco());
		l.setId(vo.getId());
		l.setLatitude(vo.getLatitude());
		l.setLongitude(vo.getLongitude());
		return l;
	}

}
