package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Produto;

@Stateless
public class ProdutoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public ProdutoDao() {
	}

	public void salvar(Produto t) {
		if (t.getId() == null)
			this.manager.persist(t);
		else
			this.alterar(t);
	}

	public void alterar(Produto t) {
		this.manager.merge(t);
	}

	public void remover(Produto t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Produto> listar() {
		try {
			return this.manager.createQuery("select p from Produto p",
					Produto.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Produto>();
		}
	}

	public Produto buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select p from Produto p where p.id = :id",
							Produto.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Produto> findByEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select p from Produto p where estabelecimento.id = :id",
							Produto.class)
					.setParameter("id", idEstabelecimento).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Produto>();
		}
	}

}
