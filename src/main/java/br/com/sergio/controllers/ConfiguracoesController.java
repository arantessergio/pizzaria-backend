package br.com.sergio.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Transactional;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Configuracao;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.persistence.novo.ConfiguracaoDao;
import br.com.sergio.persistence.novo.EstabelecimentoDao;

@Controller
public class ConfiguracoesController {

	private Result result;
	private ConfiguracaoDao configuracaoDao;
	private UsuarioLogado logado;
	private EstabelecimentoDao estabelecimentoDao;

	@Inject
	public ConfiguracoesController(Result result,
			ConfiguracaoDao configuracaoDao, UsuarioLogado logado,
			EstabelecimentoDao estabelecimentoDao) {
		this.result = result;
		this.configuracaoDao = configuracaoDao;
		this.logado = logado;
		this.estabelecimentoDao = estabelecimentoDao;
	}

	public ConfiguracoesController() {
	}
	
	public void formulario() {
		// coloca o id do estabelecimento para vincular a configuracao
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			Long id = atendenteLogado.getEstabelecimento().getId();
			String nomeAtendente = atendenteLogado.getNome();
			this.result.include("nome", nomeAtendente);
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("idEstabelecimento", id);
			List<Configuracao> configuracoes = this.configuracaoDao.listar();
			if (configuracoes != null && !configuracoes.isEmpty()) {
				Configuracao configuracao = configuracoes.get(0);
				this.result.include("configuracao", configuracao);
			}
		}
	}

	@Post
	@Transactional
	@Path(value = "/configuracoes")
	public void salvar(Configuracao configuracao) {
		Long id = configuracao.getEstabelecimento().getId();
		if (id != null) {
			Estabelecimento e = this.estabelecimentoDao.buscarPorId(id);
			configuracao.setEstabelecimento(e);
			this.configuracaoDao.salvar(configuracao);
			this.result.include("mensagem", "Configuração salva com sucesso!");
			this.result.redirectTo(InicioController.class).index();
		}
	}

	@Get
	public List<Configuracao> listar() {
		return this.configuracaoDao.listar();
	}

}
