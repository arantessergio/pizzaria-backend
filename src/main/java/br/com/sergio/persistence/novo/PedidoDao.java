package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Pedido;

@Stateless
public class PedidoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public PedidoDao() {
	}

	public void salvar(Pedido t) {
		if (t.getId() == null)
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		else
			this.alterar(t);
	}

	public void alterar(Pedido t) {
		this.manager.merge(manager.contains(t) ? t : manager.merge(t));
	}

	public void remover(Pedido t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Pedido> listar() {
		try {
			return this.manager.createQuery("select p from Pedido p",
					Pedido.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Pedido>();
		}
	}

	public List<Pedido> listarPedidosCliente(Long idCliente) {
		try {
			return this.manager
					.createQuery(
							"select p from Pedido p where p.cliente.id = :id",
							Pedido.class).setParameter("id", idCliente)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Pedido>();
		}
	}

	public Pedido buscarPedidoPorId(Long id) {
		try {
			return manager
					.createQuery("select p from Pedido p where p.id = :id",
							Pedido.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Pedido> listarPedidosPorEntregador(Long id) {
		try {
			return this.manager
					.createQuery(
							"select p from Pedido p where p.motoboy_id = :id",
							Pedido.class).setParameter("id", id)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Pedido>();
		}
	}

	public List<Pedido> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select p from Pedido p where p.estabelecimento.id = :id",
							Pedido.class).setParameter("id", idEstabelecimento)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Pedido>();
		}
	}

}
