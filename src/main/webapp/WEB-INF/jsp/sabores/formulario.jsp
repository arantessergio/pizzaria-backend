<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Sabores</h2>
				<hr>

				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[SaboresController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[SaboresController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<form class="form-horizontal"
					action="${ linkTo[SaboresController].salvar }" method="post">
					<input id="txtId" type="hidden" name="sabor.id"
						value="${ sabor.id }">
					<fieldset>
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtNome" placeholder="Descrição"
									class="form-control input-md" required="required" type="text"
									name="sabor.descricao" value="${ sabor.descricao }">
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-4">
								<textarea class="form-control" placeholder="Oque vem na pizza?"
									name="sabor.descricaoCompleta">${ sabor.descricaoCompleta }</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>

					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">${ erro }</div>
					</c:if>
					<c:if test="${not empty success}">
						<div class="alert alert-danger" role="alert">${ success }</div>
					</c:if>

				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>