<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
<title>Imprimir</title>
</head>
<body>
	<h4>Cliente</h4>
	<table class="table table-bordered">
		<tr>
			<th>Nome</th>
			<th>Telefone</th>
			<th>Cidade</th>
			<th>Bairro</th>
			<th>Rua</th>
			<th>Numero</th>
		</tr>
		<tr>
			<td><c:out value="${ pedido.cliente.nome }" /></td>
			<td><c:out value="${ pedido.cliente.telefone }" /></td>
			<td><c:out value="${ pedido.cliente.endereco.cidade.nome }" /></td>
			<td><c:out value="${ pedido.cliente.endereco.bairro.nome }" /></td>
			<td><c:out value="${ pedido.cliente.endereco.rua }" /></td>
			<td><c:out value="${ pedido.cliente.endereco.numero }" /></td>
		</tr>
	</table>
	<h4>Pizza:</h4>
	<br />
	<table class="table table-bordered">
		<tr>
			<th>Tamanho</th>
			<th>Qtde</th>
			<th>Sabores</th>
			<th>Borda</th>
			<th>Observação</th>
			<th>Total</th>
		</tr>
		<c:forEach items="${ pedido.itens }" var="item">
			<tr>
				<td><c:out value="${ item.tamanhoPizza }" /></td>
				<td><c:out value="${ item.quantidade }" /></td>
				<td><c:forEach items="${ item.sabores }" var="sabor">
												[ <c:out value="${ sabor.descricao }"></c:out> ]
											</c:forEach></td>
				<td><c:out value="${ item.borda }" /></td>
				<td><c:out value="${ item.observacao }" /></td>
				<td><c:out value="${ item.total }" /></td>
			</tr>
		</c:forEach>
	</table>
	<h4>Bebidas:</h4>
	<br />
	<table class="table table-bordered">
		<tr>
			<th>Bebida</th>
			<th>Quantidade</th>
			<th>Observação</th>
			<th>Total</th>
		</tr>
		<c:forEach items="${ pedido.itemBebidas }" var="b">
			<tr>
				<td><c:forEach items="${ b.bebidas }" var="beb">
												[ <c:out value="${ beb.descricao }" /> ]
											</c:forEach></td>
				<td><c:out value="${ b.quantidade }" /></td>
				<td><c:out value="${ b.observacao}"></c:out></td>
				<td><c:out value="${ b.total }" /></td>
			</tr>
		</c:forEach>
	</table>
	<h4>Observação:</h4>
	<h3>${ pedido.observacao }</h3>
</body>
<script src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
<script src="/javascripts/bootstrap.min.js"></script>
<script src="/javascripts/scripts_index.js"></script>
</html>