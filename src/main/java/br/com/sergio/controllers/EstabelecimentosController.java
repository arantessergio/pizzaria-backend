package br.com.sergio.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Delete;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.persistence.novo.EstabelecimentoDao;

@Controller
public class EstabelecimentosController {

	private Result result;
	private EstabelecimentoDao estabelecimentoDao;

	@Inject
	public EstabelecimentosController(Result result,
			EstabelecimentoDao estabelecimentoDao) {
		this.result = result;
		this.estabelecimentoDao = estabelecimentoDao;
	}
	
	public EstabelecimentosController() {
	}

	@Get("/estabelecimentos")
	public void listar() {
	}

	@Post("/estabelecimentos")
	public void salvar(Estabelecimento estabelecimento) {
	}

	@Delete("/estabelecimentos/{id}")
	public void remover(Long id) {

	}

}
