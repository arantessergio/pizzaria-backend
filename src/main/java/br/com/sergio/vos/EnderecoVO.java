package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Bairro;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Localizacao;

public class EnderecoVO {

	private Long id;
	private String rua;
	private Long numero;
	private Bairro bairro;
	private List<Localizacao> localizacoes;

	public EnderecoVO(Long id, String rua, Long numero, Bairro bairro,
			List<Localizacao> localizacoes) {
		super();
		this.id = id;
		this.rua = rua;
		this.numero = numero;
		this.bairro = bairro;
		this.localizacoes = localizacoes;
	}

	public Long getId() {
		return id;
	}

	public String getRua() {
		return rua;
	}

	public Long getNumero() {
		return numero;
	}

	public Bairro getBairro() {
		return bairro;
	}

	public List<Localizacao> getLocalizacoes() {
		return localizacoes;
	}

	public Endereco toEndereco(EnderecoVO vo) {
		Endereco e = new Endereco();
		e.setBairro(vo.getBairro());
		e.setId(vo.getId());
		e.setNumero(vo.getNumero());
		e.setRua(vo.getRua());
		return e;
	}

}
