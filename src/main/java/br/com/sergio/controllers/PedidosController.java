package br.com.sergio.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.carrinho.Carrinho;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bebida;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Configuracao;
import br.com.sergio.models.ESituacaoPedido;
import br.com.sergio.models.ETamanhoPizza;
import br.com.sergio.models.ETipoBorda;
import br.com.sergio.models.ETipoPedido;
import br.com.sergio.models.Entregador;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.FormaPagamento;
import br.com.sergio.models.Frete;
import br.com.sergio.models.Item;
import br.com.sergio.models.ItemBebida;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Pessoa;
import br.com.sergio.models.Sabor;
import br.com.sergio.persistence.novo.BebidaDao;
import br.com.sergio.persistence.novo.ConfiguracaoDao;
import br.com.sergio.persistence.novo.EntregadorDao;
import br.com.sergio.persistence.novo.EstabelecimentoDao;
import br.com.sergio.persistence.novo.FormaPagamentoDao;
import br.com.sergio.persistence.novo.FreteDao;
import br.com.sergio.persistence.novo.ItemBebidaDao;
import br.com.sergio.persistence.novo.ItemDao;
import br.com.sergio.persistence.novo.PedidoDao;
import br.com.sergio.persistence.novo.SaborDao;
import br.com.sergio.vos.PedidoVO;

@Controller
public class PedidosController {

	private Result result;
	private PedidoDao pedidoDao;
	private EntregadorDao entregadorDao;
	private EstabelecimentoDao estabelecimentoDao;
	private UsuarioLogado logado;
	private Carrinho carrinho;
	private SaborDao saborDao;
	private BebidaDao bebidaDao;
	private ConfiguracaoDao configDao;
	private FormaPagamentoDao formaPagamentoDao;
	private ItemDao itemDao;
	private FreteDao freteDao;
	private ItemBebidaDao itemBebidaDao;

	public PedidosController() {
	}

	@Inject
	public PedidosController(Result result, PedidoDao pedidoDao,
			EntregadorDao entregadorDao, EstabelecimentoDao estabelecimentoDao,
			UsuarioLogado logado, Carrinho carrinho, SaborDao saborDao,
			BebidaDao bebidaDao, ConfiguracaoDao configDao,
			FormaPagamentoDao formaPagamentoDao, ItemDao itemDao,
			FreteDao freteDao, ItemBebidaDao itemBebidaDao) {
		this.result = result;
		this.pedidoDao = pedidoDao;
		this.entregadorDao = entregadorDao;
		this.estabelecimentoDao = estabelecimentoDao;
		this.logado = logado;
		this.carrinho = carrinho;
		this.saborDao = saborDao;
		this.bebidaDao = bebidaDao;
		this.configDao = configDao;
		this.formaPagamentoDao = formaPagamentoDao;
		this.itemDao = itemDao;
		this.freteDao = freteDao;
		this.itemBebidaDao = itemBebidaDao;
	}

	@Get
	@Path(value = "/pedidos/{id}")
	public void edita(Long id) {
		Pessoa pessoa = this.logado.getPessoa();
		if (pessoa instanceof Atendente) {
			Atendente atendente = (Atendente) pessoa;

			String nomeFantasia = atendente.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendente.getNome();

			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
			this.result.include("atendente", true);

			Pedido pedido = this.pedidoDao.buscarPedidoPorId(id);
			if (pedido != null) {
				Long idEstabelecimento = ((Atendente) pessoa)
						.getEstabelecimento().getId();
				this.result.include("numero", pedido.getNumero());
				this.result.include("idEstabelecimento", idEstabelecimento);
				// carrega a combo dos entregadores
				List<Entregador> entregadores = this.entregadorDao.listar();
				this.result.include("entregadores", entregadores);
				// this.result.include("existeEntregador",
				// pedido.getMotoboy() == null);
				this.result.include("pedidoPronto",
						pedido.getSituacao() == ESituacaoPedido.PRONTO);
				this.result.include("idPedido", id);
				this.result.include("desconto", pedido.getDesconto() == null);
				this.result.include("entrega",
						pedido.getTipoPedido() == ETipoPedido.ENTREGA);
			}
		}
	}

	@Post("/pedidos")
	public void salvar(Pedido pedido) {
		Long idEstabelecimento = pedido.getEstabelecimento().getId();
		Estabelecimento estabelecimento = this.estabelecimentoDao
				.buscarPorId(idEstabelecimento);
		Entregador entregador = null;

		Pedido pedidoNoDB = this.pedidoDao.buscarPedidoPorId(pedido.getId());

		if (pedidoNoDB.getMotoboy() == null) {
			String nomeEntregador = pedido.getMotoboy() == null ? "" : pedido
					.getMotoboy().getNome();
			if (nomeEntregador != null && !nomeEntregador.isEmpty()) {
				entregador = this.entregadorDao.buscarPorId(Long
						.valueOf(nomeEntregador));
				pedidoNoDB.setMotoboy(entregador);
			}
		}

		if (pedido.getSituacao().toString()
				.equals(ESituacaoPedido.CANCELADO.toString())) {
			this.result.include("id", pedidoNoDB.getId());
			this.result.redirectTo(this).solicitacancelamento();
			return;
		}

		if (estabelecimento != null) {
			pedidoNoDB.setEstabelecimento(estabelecimento);
			pedidoNoDB.setAtendente((Atendente) this.logado.getPessoa());
			pedidoNoDB.setSituacao(ESituacaoPedido.valueOf(pedido.getSituacao()
					.toString()));
			if (pedido.getSituacao().toString()
					.equals(ESituacaoPedido.SAIU_ENTREGA.toString())) {
				pedidoNoDB.setHoraSaida(new Date());
			}

			if (pedido.getSituacao().toString()
					.equals(ESituacaoPedido.ENTREGUE.toString())) {
				pedidoNoDB.setHoraEntrega(new Date());
			}

			BigDecimal total = pedidoNoDB.getTotal();
			BigDecimal totalGeral = BigDecimal.ZERO;
			if (total != null && total.compareTo(BigDecimal.ZERO) > 0) {
				BigDecimal desconto = pedido.getDesconto();
				if (desconto == null)
					desconto = BigDecimal.ZERO;
				if (pedidoNoDB.getDesconto() == null) {
					totalGeral = totalGeral.add(total.subtract(desconto));
					pedidoNoDB.setTotal(totalGeral);
					pedidoNoDB.setDesconto(desconto);
				}
			}

			String tempoPreparo = pedido.getTempoPreparo();
			pedidoNoDB
					.setTempoPreparo(tempoPreparo != null ? tempoPreparo : "");

			this.pedidoDao.alterar(pedidoNoDB);
			this.result.include("success", "Pedido salvo com sucesso!");
			this.result.redirectTo(this).lista();
		}
	}

	public void solicitacancelamento() {

	}

	public void listar() {
		Long idEstabelecimento = ((Atendente) logado.getPessoa())
				.getEstabelecimento().getId();
		this.result.include("idEstabelecimento", idEstabelecimento);
	}

	public void remover(Long id) {
	}

	@Get
	@Path(value = "/pedidos", priority = Path.HIGHEST)
	public List<PedidoVO> lista() {
		Pessoa pessoa = this.logado.getPessoa();
		List<PedidoVO> resultado = new ArrayList<PedidoVO>();
		if (pessoa instanceof Atendente) {
			Atendente atendente = (Atendente) pessoa;
			if (atendente != null) {
				String nomeFantasia = atendente.getEstabelecimento()
						.getNomeFantasia();
				String nome = atendente.getNome();
				this.result.include("estabelecimento", nomeFantasia);
				this.result.include("nome", nome);
				this.result.include("atendente", true);
			}

			List<Pedido> lista = this.pedidoDao
					.listarPorEstabelecimento(((Atendente) pessoa)
							.getEstabelecimento().getId());
			for (Pedido pedido : lista) {
				resultado.add(toPedidoVo(pedido));
			}

			Collections.sort(resultado, new Comparator<PedidoVO>() {
				@Override
				public int compare(PedidoVO o1, PedidoVO o2) {
					return o1.getData().compareTo(o2.getData());
				}
			});

			return resultado;
		} else {
			Cliente cliente = (Cliente) pessoa;
			if (cliente != null) {
				String nome = cliente.getNome();
				this.result.include("nome", nome);
				this.result.include("cliente", true);
			}
			List<Pedido> lista = this.pedidoDao.listarPedidosCliente(cliente
					.getId());
			for (Pedido pedido : lista) {
				resultado.add(toPedidoVo(pedido));
			}
			Collections.sort(resultado, new Comparator<PedidoVO>() {
				@Override
				public int compare(PedidoVO o1, PedidoVO o2) {
					return o1.getData().compareTo(o2.getData());
				}
			});
			return resultado;
		}
	}

	@Get
	@Path(value = "/pedidos/{id}", priority = Path.LOW)
	public List<PedidoVO> lista(Long id) {
		List<PedidoVO> resultado = new ArrayList<PedidoVO>();
		List<Pedido> lista = this.pedidoDao.listarPedidosCliente(id);
		for (Pedido pedido : lista) {
			resultado.add(toPedidoVo(pedido));
		}
		return resultado;
	}

	public List<PedidoVO> listarPedidosCliente(Long id) {
		List<PedidoVO> resultado = new ArrayList<PedidoVO>();
		List<Pedido> pedidos = this.pedidoDao.listarPedidosCliente(id);
		for (Pedido pedido : pedidos) {
			resultado.add(toPedidoVo(pedido));
		}
		return resultado;
	}

	public void serialize(List<PedidoVO> lista) {
	}

	public PedidoVO toPedidoVo(Pedido pedido) {
		List<Item> itens = pedido.getItens();
		PedidoVO vo = new PedidoVO(pedido);
		vo.setQuantidadeItens(Long.valueOf(itens.size()));
		return vo;
	}

	public void alterarSituacaoPedido(Long id, int situacao) {
		Pedido pedido = this.pedidoDao.buscarPedidoPorId(id);
		this.pedidoDao.alterar(pedido);
	}

	public List<PedidoVO> listarPedidosPorMotoboy(Long id) {
		List<Pedido> list = this.pedidoDao.listarPedidosPorEntregador(id);
		List<PedidoVO> pedidoVOs = new ArrayList<PedidoVO>();
		for (Pedido pedido : list) {
			pedidoVOs.add(toPedidoVo(pedido));
		}
		return pedidoVOs;
	}

	public void novo(Long idEstabelecimento) {

		Pessoa pessoa = this.logado.getPessoa();
		if (idEstabelecimento != null) {
			if (carrinho.getPedido() == null) {
				Pedido pedido = new Pedido();
				pedido.setCliente((Cliente) pessoa);
				pedido.setEstabelecimento(this.estabelecimentoDao
						.buscarPorId(idEstabelecimento));
				this.carrinho.setPedido(pedido);
			} else {
				this.result.include("itemList", carrinho.getItems());
				this.result.include("formasPagamento",
						this.formaPagamentoDao.listar());
				this.result.include("bebidas", this.carrinho.getItemBebidas());
				this.result.include("tipos", ETipoPedido.values());
			}
			return;
		}
		List<Estabelecimento> estabelecimentosEstado = this.estabelecimentoDao
				.buscarPorEstado(pessoa.getEndereco().getEstado().getId());

		if (estabelecimentosEstado.size() > 1) {
			List<Estabelecimento> estabelecimentosCidade = this.estabelecimentoDao
					.buscarPorCidade(pessoa.getEndereco().getCidade().getId());

			if (estabelecimentosCidade.size() > 1) {
				redirecionaEstabelecimentos(estabelecimentosCidade);
			} else {
				inicializaPedido(estabelecimentosCidade.get(0).getId());
			}
		} else {
			if (estabelecimentosEstado.size() == 1) {
				Estabelecimento estabelecimento = estabelecimentosEstado.get(0);
				inicializaPedido(estabelecimento.getId());
			}
		}

	}

	private void redirecionaEstabelecimentos(
			List<Estabelecimento> estabelecimentosCidade) {
		this.result.include("estabelecimentos", estabelecimentosCidade);
		this.result.redirectTo(this).selecionarEstabelecimento();
	}

	private void inicializaPedido(Long idEstabelecimento) {
		if (carrinho.getPedido() == null) {
			Pedido pedido = new Pedido();
			pedido.setCliente((Cliente) logado.getPessoa());
			pedido.setEstabelecimento(this.estabelecimentoDao
					.buscarPorId(idEstabelecimento));
			this.carrinho.setPedido(pedido);
		} else {
			this.result.include("itemList", carrinho.getItems());
			this.result.include("formasPagamento",
					this.formaPagamentoDao.listar());
			this.result.include("bebidas", this.carrinho.getItemBebidas());
			this.result.include("tipos", ETipoPedido.values());
		}
	}

	public void selecionaritens() {
		List<Estabelecimento> estabelecimentosEstado = this.estabelecimentoDao
				.buscarPorEstado(this.logado.getPessoa().getEndereco()
						.getEstado().getId());

		if (estabelecimentosEstado.size() > 1) {
			List<Estabelecimento> estabelecimentosCidade = this.estabelecimentoDao
					.buscarPorCidade(this.logado.getPessoa().getEndereco()
							.getCidade().getId());

			if (estabelecimentosCidade.size() > 1) {
				redirecionaEstabelecimentos(estabelecimentosCidade);
			} else {
				if (estabelecimentosCidade.size() == 1) {
					Estabelecimento estabelecimento = estabelecimentosCidade
							.get(0);
					this.result.include("sabores", this.saborDao
							.listarPorEstabelecimento(estabelecimento.getId()));
					this.result.include("bebidas", this.bebidaDao
							.listarPorEstabelecimento(estabelecimento.getId()));
					this.result.include("tamanhos", ETamanhoPizza.values());
					this.result.include("bordas", ETipoBorda.values());
					this.result.include("id_usuario", this.logado.getPessoa()
							.getId());
				}
			}
		}
	}

	public void selecionarEstabelecimento() {
	}

	public void estabelecimentoSelecionado(String idEstabelecimento) {
		this.result.redirectTo(this).novo(Long.valueOf(idEstabelecimento));
	}

	public void selecionarbebida() {

		List<Estabelecimento> estabelecimentosEstado = this.estabelecimentoDao
				.buscarPorEstado(this.logado.getPessoa().getEndereco()
						.getEstado().getId());

		if (estabelecimentosEstado.size() > 1) {
			List<Estabelecimento> estabelecimentosCidade = this.estabelecimentoDao
					.buscarPorCidade(this.logado.getPessoa().getEndereco()
							.getCidade().getId());

			if (estabelecimentosCidade.size() > 1) {
				redirecionaEstabelecimentos(estabelecimentosCidade);
			} else {
				this.result.include("bebidas", this.bebidaDao.listar());
			}
		}

	}

	@Post("/pedidos/item")
	public void additem(String sabor01, String sabor02, String sabor03,
			String sabor04, String tamanhoPizza, int quantidadePizza,
			int quantidadeBebida, String nomeBebida, String observacao,
			String borda) {

		Item item = new Item();
		List<Sabor> sabores = new ArrayList<Sabor>();

		Sabor s1 = null;
		Sabor s2 = null;
		Sabor s3 = null;
		Sabor s4 = null;

		if (validaSabor(sabor01)) {
			s1 = this.saborDao.buscarPorId(Long.valueOf(sabor01));
			sabores.add(s1);
		}
		if (validaSabor(sabor02)) {
			s2 = this.saborDao.buscarPorId(Long.valueOf(sabor02));
			sabores.add(s2);
		}
		if (validaSabor(sabor03)) {
			s3 = this.saborDao.buscarPorId(Long.valueOf(sabor03));
			sabores.add(s3);
		}
		if (validaSabor(sabor04)) {
			s4 = this.saborDao.buscarPorId(Long.valueOf(sabor04));
			sabores.add(s4);
		}

		List<Configuracao> configs = this.configDao.listar();
		Configuracao config = null;

		if (configs.size() > 0)
			config = configs.get(0);

		BigDecimal valorGigante = BigDecimal.ZERO;
		BigDecimal valorPequena = BigDecimal.ZERO;
		BigDecimal valorMedia = BigDecimal.ZERO;
		BigDecimal valorGrande = BigDecimal.ZERO;
		BigDecimal valorGiganteBorda = BigDecimal.ZERO;
		if (tamanhoPizza.equals(ETamanhoPizza.PEQUENA.toString())) {
			valorPequena = config.getValorPequena();
		} else if (tamanhoPizza.equals(ETamanhoPizza.MEDIA.toString())) {
			valorMedia = config.getValorMedia();
		} else if (tamanhoPizza.equals(ETamanhoPizza.GRANDE.toString())) {
			valorGrande = config.getValorGrande();
		} else if (tamanhoPizza.equals(ETamanhoPizza.EXTRA_GRANDE.toString())) {
			valorGigante = config.getValorGigante();
		} else if (tamanhoPizza.equals(ETamanhoPizza.EXTRA_GRANDE_BORDA
				.toString())) {
			valorGiganteBorda = config.getValorGiganteBorda();
		}

		item.setSabores(sabores);

		item.setTamanhoPizza(ETamanhoPizza.valueOf(tamanhoPizza));
		item.setQuantidade(Long.valueOf(quantidadePizza));

		item.setBorda(borda != null ? ETipoBorda.valueOf(borda)
				: ETipoBorda.SEM_BORDA_RECHEADA);

		BigDecimal total = BigDecimal.ZERO;

		if (tamanhoPizza.equals(ETamanhoPizza.PEQUENA.toString())) {
			BigDecimal totalPizza = valorPequena.multiply(BigDecimal
					.valueOf(quantidadePizza));
			total = total.add(totalPizza);
			this.carrinho.addItem(item);
		} else if (tamanhoPizza.equals(ETamanhoPizza.MEDIA.toString())) {
			BigDecimal totalPizza = valorMedia.multiply(BigDecimal
					.valueOf(quantidadePizza));
			total = total.add(totalPizza);
			this.carrinho.addItem(item);
		} else if (tamanhoPizza.equals(ETamanhoPizza.GRANDE.toString())) {
			BigDecimal totalPizza = valorGrande.multiply(BigDecimal
					.valueOf(quantidadePizza));
			total = total.add(totalPizza);
			this.carrinho.addItem(item);
		} else if (tamanhoPizza.equals(ETamanhoPizza.EXTRA_GRANDE.toString())) {
			BigDecimal totalPizza = valorGigante.multiply(BigDecimal
					.valueOf(quantidadePizza));
			total = total.add(totalPizza);
			this.carrinho.addItem(item);
		} else if (tamanhoPizza.equals(ETamanhoPizza.EXTRA_GRANDE_BORDA
				.toString())) {
			BigDecimal totalPizza = valorGiganteBorda.multiply(BigDecimal
					.valueOf(quantidadePizza));
			total = total.add(totalPizza);
			this.carrinho.addItem(item);
		}

		if (borda == null) {
			item.setBorda(ETipoBorda.SEM_BORDA_RECHEADA);
		}

		if (borda != null
				&& !ETipoBorda.valueOf(borda).equals(
						ETipoBorda.SEM_BORDA_RECHEADA)) {
			total = total.add(config.getValorBorda());
		}

		item.setTotal(total);
		item.setObservacao(observacao);

		this.result.include("mensagem", "Item adicionado ao carrinho!");
		this.result.redirectTo(this).novo(
				item.getPedido().getEstabelecimento().getId());
	}

	private boolean validaSabor(String sabor) {
		if (sabor != null && !sabor.isEmpty()) {
			return true;
		}
		return false;
	}

	@Post("/pedidos/item/bebida")
	public void addBebida(String bebida, String quantidade, String observacao) {
		ItemBebida b = new ItemBebida();

		Bebida beb = null;
		if (bebida != null && !bebida.isEmpty()) {
			beb = this.bebidaDao.buscarPorId(Long.valueOf(bebida));
		}

		List<Bebida> bebidas = new ArrayList<Bebida>();
		bebidas.add(beb);
		b.setBebidas(bebidas);
		b.setQuantidade(Long.valueOf(quantidade));
		b.setObservacao(observacao);
		b.setTotal(beb.getValorVenda().multiply(
				BigDecimal.valueOf(Long.valueOf(quantidade))));

		this.carrinho.addItemBebida(b);
		this.result.include("mensagem", "Item adicionado ao carrinho!");
		this.result.redirectTo(this).novo(
				carrinho.getPedido().getEstabelecimento().getId());
	}

	public void enviar(String formaPagamento, String tipoPedido,
			String observacao) {

		if (carrinho.getItems().size() == 0) {
			this.result
					.include("erro", "Por favor selecione ao menos um item!");
			this.result.redirectTo(this).selecionaritens();
			return;
		}

		Pedido p = this.carrinho.getPedido();

		FormaPagamento pgto = this.formaPagamentoDao.buscarPorId(Long
				.valueOf(formaPagamento));

		p.setCliente((Cliente) this.logado.getPessoa());
		p.setHoraRealizacao(new Date());
		p.setData(new Date());
		p.setSituacao(ESituacaoPedido.REALIZADO);
		p.setFormaPagamento(pgto);

		p.setTipoPedido(ETipoPedido.valueOf(tipoPedido));

		List<Item> items = this.carrinho.getItems();
		p.setItens(items);

		List<ItemBebida> itemBebidas = this.carrinho.getItemBebidas();
		p.setItemBebidas(itemBebidas);

		BigDecimal total = BigDecimal.ZERO;

		for (Item item : items) {
			total = total.add(item.getTotal() != null ? item.getTotal()
					: BigDecimal.ZERO);
		}

		for (ItemBebida i : itemBebidas) {
			total = total.add(i.getTotal());
		}

		BigDecimal valorFrete = BigDecimal.ZERO;

		if (ETipoPedido.valueOf(tipoPedido).equals(ETipoPedido.ENTREGA)) {
			Frete frete = this.freteDao.buscarPorBairro(this.logado.getPessoa()
					.getEndereco().getBairro().getId());
			if (frete != null)
				valorFrete = frete.getValor();
		}
		total = total.add(valorFrete);
		p.setTotal(total);
		p.setObservacao(observacao);
		this.pedidoDao.salvar(p);
		this.result
				.include("success",
						"Pedido realizado com sucesso, por favor acompanhe nosso retorno, OBRIGADO!");
		this.result.redirectTo(this).lista();
		this.carrinho.removeAllItems();
	}

	@Path("/pedidos/items/{id}")
	public void veritens(Long id) {
		this.result.include("cliente", (logado.getPessoa() instanceof Cliente));
		this.result.include("atendente",
				(logado.getPessoa() instanceof Atendente));
		this.result.include("itemPizzas", this.itemDao.listarPorPedido(id));
		this.result.include("itemBebidas",
				this.itemBebidaDao.listarPorPedido(id));
	}

	@Path("/pedidos/cancelar/{id}")
	public void cancelar(Long id) {
		Pedido pedidoNoDb = this.pedidoDao.buscarPedidoPorId(id);
		if (pedidoNoDb != null) {
			pedidoNoDb.setSituacao(ESituacaoPedido.CANCELADO);
			pedidoNoDb.setAtendente((Atendente) this.logado.getPessoa());
			this.result.include("cancelado", true);
			this.pedidoDao.alterar(pedidoNoDb);
			this.result.include("success", "Pedido alterado com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

	public void limparcarrinho() {
		this.carrinho.removeAllItems();
		this.result.redirectTo(this).novo(
				carrinho.getPedido().getEstabelecimento().getId());
	}

	public void limparbebidas() {
		this.carrinho.removeAllItemBebidas();
		this.result.redirectTo(this).novo(
				carrinho.getPedido().getEstabelecimento().getId());
	}

	@Path("/pedidos/imprimir/{id}")
	public void imprimir(Long id) {
		Pedido pedido = this.pedidoDao.buscarPedidoPorId(id);
		if (pedido != null) {
			this.result.include("pedido", pedido);
		} else {
			this.result.notFound();
		}
	}

	/**
	 * @Path("/pedidos/imprimir/{id ") public void imprimir(Long id) { Pedido
	 *                              pedido =
	 *                              this.pedidoDao.buscarPedidoPorId(id); if
	 *                              (pedido != null) {
	 *                              this.result.include("pedido", pedido);
	 * 
	 *                              List<Item> itens = pedido.getItens();
	 *                              StringBuilder builder = new StringBuilder();
	 *                              builder.append("Itens: ");
	 *                              builder.append("\n"); for (Iterator iterator
	 *                              = itens.iterator(); iterator.hasNext();) {
	 *                              Item item = (Item) iterator.next();
	 *                              builder.append("Sabores: "); List<Sabor>
	 *                              sabores = item.getSabores(); for (Iterator
	 *                              it = sabores.iterator(); it.hasNext();) {
	 *                              Sabor sabor = (Sabor) it.next();
	 *                              builder.append(sabor.getDescricao()); if
	 *                              (it.hasNext()) { builder.append(" - "); } }
	 *                              // builder.append("\n"); //
	 *                              builder.append("Bebidas: "); // List<Bebida>
	 *                              bebidas = item.getBebidas(); // for
	 *                              (Iterator hue = bebidas.iterator();
	 *                              hue.hasNext();) { // Bebida bebida =
	 *                              (Bebida) hue.next(); //
	 *                              builder.append(bebida.getDescricao()); //
	 *                              builder.append("Qtde: ("); //
	 *                              builder.append(item.getQuantidadeBebida());
	 *                              // builder.append(" )"); // if
	 *                              (hue.hasNext()) { // builder.append(" - ");
	 *                              // } // } if (iterator.hasNext()) {
	 *                              builder.append(" --- "); } }
	 * 
	 *                              this.result.include("itens",
	 *                              builder.toString()); } else {
	 *                              this.result.notFound(); } }
	 * 
	 *                              /** private List<Sabor>
	 *                              addSabores(List<String> sabores) {
	 *                              List<Sabor> retorno = new
	 *                              ArrayList<Sabor>(); Sabor s01 =
	 *                              this.saborDao
	 *                              .buscarPorId(Long.valueOf(sabores.get(0)));
	 *                              retorno.add(s01); for (int i = 0; i <
	 *                              sabores.size(); i++) { String sabor =
	 *                              sabores.get(0); if (i < sabores.size() - 1)
	 *                              { if (!sabor.equals(sabores.get(i + 1))) {
	 *                              Sabor s =
	 *                              this.saborDao.buscarPorId(Long.valueOf
	 *                              (sabor)); retorno.add(s); } } } return
	 *                              retorno; }
	 */
}
