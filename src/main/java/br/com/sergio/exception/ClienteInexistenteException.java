package br.com.sergio.exception;

public class ClienteInexistenteException extends RuntimeException {

	private static final long serialVersionUID = 1824936155174693916L;

	public ClienteInexistenteException(String message) {
		super(message);
	}

}
