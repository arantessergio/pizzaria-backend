package br.com.sergio.exception;

public class UsuarioExistenteException extends RuntimeException {

	private static final long serialVersionUID = -487406031736136421L;

	public UsuarioExistenteException(String message) {
		super(message);
	}

}
