package br.com.sergio.vos;

import java.math.BigDecimal;

import br.com.sergio.models.Item;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Produto;

public class ItemVO {

	private Long id;
	private Pedido pedido;
	private Produto produto;
	private Long quantidade;
	private BigDecimal total;

	public ItemVO(Long id, Pedido pedido, Produto produto, Long quantidade,
			BigDecimal total) {
		super();
		this.id = id;
		this.pedido = pedido;
		this.produto = produto;
		this.quantidade = quantidade;
		this.total = total;
	}

	public Long getId() {
		return id;
	}

	public Pedido getPedido() {
		return pedido;
	}

	public Produto getProduto() {
		return produto;
	}

	public Long getQuantidade() {
		return quantidade;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public Item toItem(ItemVO vo) {
		Item i = new Item();
		i.setId(vo.getId());
		i.setPedido(vo.getPedido());
//		i.setProduto(vo.getProduto());
		i.setQuantidade(vo.getQuantidade());
		i.setTotal(vo.getTotal());
		return i;
	}

}
