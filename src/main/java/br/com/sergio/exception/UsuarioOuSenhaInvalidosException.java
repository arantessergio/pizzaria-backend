package br.com.sergio.exception;

public class UsuarioOuSenhaInvalidosException extends RuntimeException {

	private static final long serialVersionUID = -7842597517376856596L;

	public UsuarioOuSenhaInvalidosException(String message) {
		super(message);
	}

}
