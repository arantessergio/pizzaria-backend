<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Pedidos</h2>
				<hr>
				<div class="table-responsive">
					<table class="table table-bordered">
						<tr>
							<th>Cliente</th>
							<th>Motoboy</th>
							<th>Atendente</th>
							<th>Situação</th>
							<th>Forma pagamento</th>
							<th>Data</th>
							<th>Hora entrega</th>
							<th>Hora realização</th>
							<th>Hora saída</th>
							<th>Total</th>
							<th>Qtde. Itens</th>
							<th>Qtde. Produtos</th>
							<th></th>
						</tr>
						<c:forEach items="${ pedidoVOList }" var="pedido">
							<tr>
								<td><c:out value="${ pedido.cliente.nome }" /></td>
								<td><c:out value="${ pedido.motoboy.nome }" /></td>
								<td><c:out value="${ pedido.atendente.nome }" /></td>
								<td><c:out value="${ pedido.situacao }" /></td>
								<td><c:out value="${ pedido.formaPagamento.descricao }" /></td>
								<td><c:out value="${ pedido.data }" /></td>
								<td><c:out value="${ pedido.horaEntrega }" /></td>
								<td><c:out value="${ pedido.horaRealizacao }" /></td>
								<td><c:out value="${ pedido.horaSaida }" /></td>
								<td><c:out value="${ pedido.total }" /></td>
								<td><c:out value="${ pedido.quantidadeItens }" /></td>
								<td><c:out value="${ pedido.quantidadeProdutos }" /></td>
								<td>
									<button id="itens" name="itens" class="btn btn-info"
										type="button">Editar</button>
								</td>
							</tr>
						</c:forEach>
					</table>
				</div>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>