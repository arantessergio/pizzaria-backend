package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.EStatus;
import br.com.sergio.models.Endereco;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Localizacao;
import br.com.sergio.models.Entregador;
import br.com.sergio.models.Pedido;
import br.com.sergio.models.Telefone;

public class MotoboyVO extends PessoaVO {

	private List<Localizacao> localizacoes;
	private List<Pedido> pedidos;
	private List<Telefone> telefones;
	private Estabelecimento estabelecimento;

	public MotoboyVO(Long id, String nome, String rg, String cpf,
			EStatus status, String login, String senha, Endereco endereco) {
		super(id, nome, rg, cpf, status, login, senha, endereco);
	}

	public List<Localizacao> getLocalizacoes() {
		return localizacoes;
	}

	public List<Pedido> getPedidos() {
		return pedidos;
	}

	public List<Telefone> getTelefones() {
		return telefones;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public Entregador toMotoboy(MotoboyVO vo) {
		Entregador m = new Entregador();
		m.setCpf(vo.getCpf());
		m.setEndereco(vo.getEndereco());
		m.setEstabelecimento(vo.getEstabelecimento());
		m.setId(vo.getId());
		m.setLocalizacoes(vo.getLocalizacoes());
		m.setLogin(vo.getLogin());
		m.setNome(vo.getNome());
		m.setPedidos(vo.getPedidos());
		m.setRg(vo.getRg());
		m.setSenha(vo.getSenha());
		m.setStatus(vo.getStatus());
		m.setTelefones(vo.getTelefones());
		return m;
	}

}
