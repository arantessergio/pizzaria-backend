package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Item;

@Stateless
public class ItemDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public ItemDao() {
	}

	public void salvar(Item t) {
		if (t.getId() == null) {
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		} else {
			this.alterar(t);
		}
	}

	public void alterar(Item t) {
		this.manager.merge(t);
	}

	public void remover(Item t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Item> listarPorPedido(Long idPedido) {
		try {
			return this.manager.createQuery("select i from Item i where i.pedido.id = :id",
					Item.class)
					.setParameter("id", idPedido)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Item>();
		}
	}

	public Item buscarPorIdPedido(Long id) {
		try {
			return this.manager
					.createQuery(
							"select i from Item i where i.pedido.id = :id",
							Item.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
