<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menuclientes.jsp" />
			<div class="col-sm-9">
				<h2>Novo pedido</h2>
				<hr>

				<form class="form-horizontal"
					action="${ linkTo[PedidosController].enviar }" method="post">
					<fieldset>
						<input id="txtId" type="hidden" name="pedido.id"
							value="${ pedido.id }">
						<c:if test="${ not empty id_estabelecimento }">
							<input type="hidden" name="pedido.estabelecimento.id"
								value="${ id_estabelecimento }">
						</c:if>
						<h4>Pizzas:</h4>
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<th>Tamanho</th>
									<th>Qtde</th>
									<th>Sabores</th>
									<th>Borda</th>
									<th>Observação</th>
									<th>Total</th>
								</tr>
								<c:forEach items="${ itemList }" var="item">
									<tr>
										<td><c:out value="${ item.tamanhoPizza }" /></td>
										<td><c:out value="${ item.quantidade }" /></td>
										<td><c:forEach items="${ item.sabores }" var="sabor">
												[ <c:out value="${ sabor.descricao }"></c:out> ]
											</c:forEach></td>
										<td><c:out value="${ item.borda }" /></td>
										<td><c:out value="${ item.observacao }" /></td>
										<td><c:out value="${ item.total }" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<a href="${ linkTo[PedidosController].selecionaritens }"
									class="btn btn-success">Adicionar pizza</a>
							</div>
						</div>
						<hr>
						<h4>Bebidas:</h4>
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<th>Bebida</th>
									<th>Quantidade</th>
									<th>Observação</th>
									<th>Total</th>
								</tr>
								<c:forEach items="${ bebidas }" var="b">
									<tr>
										<td><c:forEach items="${ b.bebidas }" var="beb">
												[ <c:out value="${ beb.descricao }" /> ]
											</c:forEach></td>
										<td><c:out value="${ b.quantidade }" /></td>
										<td><c:out value="${ b.observacao}"></c:out></td>
										<td><c:out value="${ b.total }" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<a href="${ linkTo[PedidosController].selecionarbebida }"
									class="btn btn-success">Adicionar bebida</a>
							</div>
						</div>
						<hr>
						Forma de pagamento:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor01" name="formaPagamento" class="form-control">
									<c:forEach items="${ formasPagamento }" var="forma">
										<option value="${ forma.id }">${ forma.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<hr>
						Tipo pedido:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="tipoPedido" name="tipoPedido" class="form-control">
									<c:forEach items="${ tipos }" var="tipo">
										<option value="${ tipo }">${ tipo }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<hr>
						<div class="form-group">
							<div class="col-md-4">
								<textarea class="form-control" placeholder="Observação"
									name="observacao">${ observacao }</textarea>
							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<button type="submit" class="btn btn-success">Finalizar</button>
								<a href="${ linkTo[PedidosController].limparcarrinho }"
									class="btn btn-danger">Limpar carrinho</a>
							</div>
						</div>
					</fieldset>
				</form>
				<c:if test="${not empty mensagem}">
					<div class="alert alert-success" role="alert">${ mensagem }</div>
				</c:if>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
	<script type="text/javascript">

		alert("Lembramos que nosso horário de atendimento é de Quinta à Terça das 18:30 às 23:30, Obrigado!")

	</script>
</body>
</html>