<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="br.com.sergio.models.ETamanhoPizza"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menuclientes.jsp" />
			<div class="col-sm-9">
				<h2>Adicionar pizza</h2>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">${ erro }</div>
					</c:if>
				</div>
				<form class="form-horizontal"
					action="${ linkTo[PedidosController].additem }" method="post">
					<fieldset>
						Tamanho da pizza:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="cmbTamanho" name="tamanhoPizza" class="form-control">
									<c:forEach items="${ tamanhos }" var="tamanho">
										<option value="${ tamanho }">${ tamanho }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Sabor 01:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor01" name="sabor01" class="form-control">
									<c:forEach items="${ sabores }" var="sabor">
										<option value="${ sabor.id }">${ sabor.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Sabor 02:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor02" name="sabor02" class="form-control">
									<c:forEach items="${ sabores }" var="sabor">
										<option value="${ sabor.id }">${ sabor.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Sabor 03:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor03" name="sabor03" class="form-control">
									<c:forEach items="${ sabores }" var="sabor">
										<option value="${ sabor.id }">${ sabor.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Sabor 04:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor04" name="sabor04" class="form-control">
									<c:forEach items="${ sabores }" var="sabor">
										<option value="${ sabor.id }">${ sabor.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Quantidade:<br />
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtQuantidadeB" placeholder="Quantidade pizzas"
									class="form-control input-md" required="required" type="number"
									name="quantidadePizza" value="${ item.quantidade }">

							</div>
						</div>
						<br> Borda:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="borda" name="borda" class="form-control">
									<c:forEach items="${ bordas }" var="borda">
										<option value="${ borda }">${ borda }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Observação:<br />
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtQuantidadeB" placeholder="Observação"
									class="form-control input-md" type="text"
									name="observacao" value="${ item.observacao }">

							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Adicionar</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<script src="../javascripts/jquery-1.11.2.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
	<script type="text/javascript">
		var comboTamanho = $("#cmbTamanho");
		var comboSabor01 = $("#sabor01");
		var comboSabor02 = $("#sabor02");
		var comboSabor03 = $("#sabor03");
		var comboSabor04 = $("#sabor04");
		var comboBorda = $("#borda");

		desabilitaCombo(comboSabor02, '', true);
		desabilitaCombo(comboSabor03, '', true);
		desabilitaCombo(comboSabor04, '', true);
		
		comboTamanho.on('change', function() {
			switch (String(comboTamanho.val())) {
			case 'PEQUENA':
				desabilitaCombo(comboSabor02, '', true);
				desabilitaCombo(comboSabor03, '', true);
				desabilitaCombo(comboSabor04, '', true);
				desabilitaCombo(comboBorda, '', false);
				break;
			case 'MEDIA':
				desabilitaCombo(comboSabor03, '', true);
				desabilitaCombo(comboSabor04, '', true);
				desabilitaCombo(comboSabor01, '', false);
				desabilitaCombo(comboSabor02, '', false);
				desabilitaCombo(comboBorda, '', false);
				break;
			case 'GRANDE':
				desabilitaCombo(comboSabor04, '', true);
				desabilitaCombo(comboSabor01, '', false);
				desabilitaCombo(comboSabor02, '', false);
				desabilitaCombo(comboSabor03, '', false);
				desabilitaCombo(comboBorda, '', false);
				break;
			case 'EXTRA_GRANDE':
				desabilitaCombo(comboSabor01, '', false);
				desabilitaCombo(comboSabor02, '', false);
				desabilitaCombo(comboSabor03, '', false);
				desabilitaCombo(comboSabor04, '', false);
				desabilitaCombo(comboBorda, '', false);
				break;
			case 'EXTRA_GRANDE_BORDA':
				desabilitaCombo(comboSabor01, '', false);
				desabilitaCombo(comboSabor02, '', false);
				desabilitaCombo(comboSabor03, '', false);
				desabilitaCombo(comboSabor04, '', false);
				desabilitaCombo(comboBorda, '', true);
				break;
			}
		});

		function desabilitaCombo(combo, val, ativo) {
			combo.prop('disabled', ativo);
			combo.val(val);
		}
	</script>
</body>
</html>