package br.com.sergio.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.exception.CpfInvalidoException;
import br.com.sergio.exception.SenhaInvalidaException;
import br.com.sergio.exception.UsuarioExistenteException;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Estado;
import br.com.sergio.persistence.novo.AtendenteDao;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.CidadeDao;
import br.com.sergio.persistence.novo.EnderecoDao;
import br.com.sergio.persistence.novo.EstabelecimentoDao;
import br.com.sergio.persistence.novo.EstadoDao;

@Controller
public class AtendentesController {

	private Result result;
	private AtendenteDao atendenteDao;
	private EstadoDao estadoDao;
	private CidadeDao cidadeDao;
	private BairroDao bairroDao;
	private EnderecoDao enderecoDao;
	private EstabelecimentoDao estabelecimentoDao;
	private UsuarioLogado logado;

	@Inject
	public AtendentesController(Result result, AtendenteDao atendenteDao,
			EstadoDao estadoDao, CidadeDao cidadeDao, BairroDao bairroDao,
			EnderecoDao enderecoDao, EstabelecimentoDao estabelecimentoDao,
			UsuarioLogado logado) {
		this.result = result;
		this.atendenteDao = atendenteDao;
		this.estadoDao = estadoDao;
		this.cidadeDao = cidadeDao;
		this.bairroDao = bairroDao;
		this.enderecoDao = enderecoDao;
		this.estabelecimentoDao = estabelecimentoDao;
		this.logado = logado;
	}

	public AtendentesController() {
	}

	@Path("/atendentes/formulario")
	public void formulario() {
		List<Bairro> listaBairro = bairroDao.listar();
		List<Cidade> listaCidade = cidadeDao.listar();
		List<Estado> listaEstado = estadoDao.listar();
		// carrega as combos
		this.result.include("estados", listaEstado);
		this.result.include("cidades", listaCidade);
		this.result.include("bairros", listaBairro);

		Atendente atendente = (Atendente) this.logado.getPessoa();

		if (atendente != null) {
			Long idEstabelecimento = atendente.getEstabelecimento().getId();
			this.result.include("idEstabelecimento", idEstabelecimento);
			this.result.include("estabelecimento", atendente
					.getEstabelecimento().getNomeFantasia());
			this.result.include("nome", atendente.getNome());
		}

	}

	@Post("/atendentes")
	public void salvar(Atendente atendente) {
		try {
			validaSenha(atendente.getSenha());
			validaCpf(atendente.getCpf());
			validaUsuario(atendente.getLogin());
			String nomeCidade = atendente.getEndereco().getBairro().getCidade()
					.getNome();
			String nomeEstado = atendente.getEndereco().getBairro().getCidade()
					.getEstado().getNome();
			String nomeBairro = atendente.getEndereco().getBairro().getNome();

			Estado estado = estadoDao.buscarPorNome(nomeEstado);
			Cidade cidade = cidadeDao.buscarPorNome(nomeCidade, estado.getId());
			Bairro bairro = bairroDao.buscarPorNome(nomeBairro, cidade.getId());

			atendente.getEndereco().setBairro(bairro);
			atendente.getEndereco().setCidade(cidade);
			atendente.getEndereco().setEstado(estado);

			enderecoDao.salvar(atendente.getEndereco());

			Long idEstabelecimento = ((Atendente) this.logado.getPessoa())
					.getEstabelecimento().getId();
			Estabelecimento estabelecimento = this.estabelecimentoDao
					.buscarPorId(idEstabelecimento);
			if (estabelecimento != null) {
				atendente.setEstabelecimento(estabelecimento);
			}

			this.atendenteDao.salvar(atendente);
			this.result.include("sucesso", "Atendente salvo com sucesso!").of(
					this);

		} catch (SenhaInvalidaException sE) {
			this.result.include("senhaInvalida", sE.getMessage())
					.redirectTo(this).formulario();
		} catch (UsuarioExistenteException uE) {
			this.result.include("usuarioInvalido", uE.getMessage())
					.redirectTo(this).formulario();
		} catch (CpfInvalidoException e) {
			this.result.include("cpfInvalido", e.getMessage()).redirectTo(this)
					.formulario();
		} catch (Exception e) {
			this.result.redirectTo(ErroController.class).paginaerro(
					e.getMessage());
			e.printStackTrace();
		}
	}

	private void validaUsuario(Object login) throws UsuarioExistenteException {
		List<Atendente> list = atendenteDao.listar();
		for (Atendente a : list) {
			if (a.getLogin().equals(login)) {
				throw new UsuarioExistenteException(
						"Já existe um atendente com este login, verifique!");
			}
		}
	}

	/**
	 * faz o calculo de digito verificador e valida o cpf
	 */
	private void validaCpf(String cpf) throws CpfInvalidoException {
		if (cpf == null) {
			throw new CpfInvalidoException(
					"O CPF informado é invalido, verifique!");
		}
	}

	/**
	 * valida se a senha é maior que seis caracteres
	 */
	private void validaSenha(String senha) throws SenhaInvalidaException {
		char[] senhaArray = senha.toCharArray();
		if (senhaArray.length < 6) {
			throw new SenhaInvalidaException(
					"Sua senha não pode ser menor que 6 caracteres!");
		}
	}

	@Get("/atendentes")
	public List<Atendente> lista() {
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendenteLogado.getNome();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
		}
		return this.atendenteDao.listarPorEstabelecimento(atendenteLogado
				.getEstabelecimento().getId());
	}

	@Get
	@Path(value = "/atendentes/{id}", priority = Path.HIGHEST)
	public void edita(Long id) {
		Atendente atendente = this.atendenteDao.buscarPorId(id);
		this.result.include("atendente", atendente);
		this.result.redirectTo(this).formulario();
	}

	@Path(value = "/atendentes/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Atendente atendente = this.atendenteDao.buscarPorId(id);
		if (atendente != null) {
			if (logado.getPessoa().getId().compareTo(atendente.getId()) == 0) {
				this.result.include("erro",
						"Não é possível excluir seu próprio usuário!");
				this.result.redirectTo(this).erroexcluir();
				return;
			}
			this.result.include("id", atendente.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void erroexcluir() {

	}

	public void confirmaexclusao() {
	}

	@Path("/atendentes/excluir/{id}")
	public void excluir(Long id) {
		Atendente atendente = atendenteDao.buscarPorId(id);
		if (atendente != null) {
			this.atendenteDao.remover(atendente);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

}
