package br.com.sergio.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Transactional;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bebida;
import br.com.sergio.models.Cliente;
import br.com.sergio.models.Estabelecimento;
import br.com.sergio.models.Pessoa;
import br.com.sergio.persistence.novo.BebidaDao;

@Controller
public class BebidasController {

	private BebidaDao dao;
	private UsuarioLogado logado;
	private Result result;

	@Inject
	public BebidasController(BebidaDao dao, UsuarioLogado logado,
			Result result) {
		this.dao = dao;
		this.logado = logado;
		this.result = result;
	}

	public BebidasController() {
	}

	public void formulario() {
		inserirInformacoesCabecalho();
	}

	private void inserirInformacoesCabecalho() {
		Pessoa pessoa = this.logado.getPessoa();
		if (pessoa instanceof Atendente) {
			Atendente atendente = (Atendente) pessoa;
			if (atendente != null) {
				Long idEstabelecimento = atendente.getEstabelecimento().getId();
				this.result.include("idEstabelecimento", idEstabelecimento);
				this.result.include("estabelecimento", atendente
						.getEstabelecimento().getNomeFantasia());
				this.result.include("nome", atendente.getNome());
				this.result.include("atendente", true);
			}
		} else {
			Cliente cliente = (Cliente) pessoa;
			if (cliente != null) {
				this.result.include("nome", cliente.getNome());
				this.result.include("cliente", true);
			}
		}
	}

	@Transactional
	public void salvar(Bebida bebida) {
		if (bebida != null) {
			Estabelecimento estabelecimento = ((Atendente)this.logado.getPessoa()).getEstabelecimento();
			if (estabelecimento != null) {
				bebida.setEstabelecimento(estabelecimento);
				this.dao.salvar(bebida);
				this.result.include("success", "Item adicionado com sucesso!");
				this.result.redirectTo(this).lista();
			} else {
				this.result.notFound();
			}
		} else {
			this.result.notFound();
		}
	}

	@Path(value = "/bebidas/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Bebida bebida = this.dao.buscarPorId(id);
		if (bebida != null) {
			this.result.include("id", bebida.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void confirmaexclusao() {
	}

	@Path("/bebidas/excluir/{id}")
	public void excluir(Long id) {
		Bebida bebida = dao.buscarPorId(id);
		if (bebida != null) {
			this.dao.excluir(bebida);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

	@Path(value = "/bebidas/{id}", priority = Path.LOW)
	public void editar(Long id) {
		Bebida bebida = this.dao.buscarPorId(id);
		if (bebida != null) {
			this.result.include(bebida);
			this.result.redirectTo(this).formulario();
		} else {
			this.result.notFound();
		}
	}

	@Get("/bebidas")
	public List<Bebida> lista() {
		inserirInformacoesCabecalho();
		Long idEstabelecimento = ((Atendente)this.logado.getPessoa()).getEstabelecimento().getId();
		return dao.listarPorEstabelecimento(idEstabelecimento);
	}

}
