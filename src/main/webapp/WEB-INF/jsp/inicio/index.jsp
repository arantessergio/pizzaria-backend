<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<c:if test="${ atendente }">
				<jsp:include page="../templates/menulateral.jsp" />
			</c:if>
			<c:if test="${ cliente }">
				<jsp:include page="../templates/menuclientes.jsp" />
			</c:if>
			<div class="col-sm-9">
				<c:if test="${ not empty nome }">
					<h3>
						Bem vindo <br /> <small>${ nome }</small>
					</h3>
					<hr>
					<c:if test="${ cliente }">
						<h4>
							Contato: <br /> <small>(45) 3035-3217</small>
						</h4>
						<hr>
						<h4>O Frete para o seu bairro é R$ ${ frete }</h4>
						<hr>
						<h4>Confira nossos preços</h4>
						<div class="table-responsive">
							<table class="table table-bordered">
								<tr>
									<th>Pequena (1 Sabor)</th>
									<th>Média (2 Sabores)</th>
									<th>Grande (3 Sabores)</th>
									<th>Extra grande (4 Sabores)</th>
									<th>Extra grande + Borda recheada + Refri</th>
								</tr>
								<c:forEach items="${ configs }" var="config">
									<tr>
										<td><c:out value="${ config.valorPequena }" /></td>
										<td><c:out value="${ config.valorMedia }" /></td>
										<td><c:out value="${ config.valorGrande }" /></td>
										<td><c:out value="${ config.valorGigante }" /></td>
										<td><c:out value="${ config.valorGiganteBorda }" /></td>
									</tr>
								</c:forEach>
							</table>
						</div>
					</c:if>
				</c:if>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>