package br.com.sergio.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Pedido implements Serializable {

	private static final long serialVersionUID = 3831092600796952150L;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private Long id;
	private String numero;
	@ManyToOne
	@JoinColumn(name = "cliente_id")
	private Cliente cliente;
	@ManyToOne
	@JoinColumn(name = "entregador_id")
	private Entregador motoboy;
	@ManyToOne
	@JoinColumn(name = "atendente_id")
	private Atendente atendente;
	@Enumerated(EnumType.STRING)
	private ESituacaoPedido situacao;
	@OneToMany(mappedBy = "pedido", targetEntity = Item.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Item> itens;
	@ManyToOne
	@JoinColumn(name = "estabelecimento_id")
	private Estabelecimento estabelecimento;
	@ManyToOne
	@JoinColumn(name = "formapagamento_id")
	private FormaPagamento formaPagamento;
	@Temporal(TemporalType.DATE)
	private Date data;
	@Temporal(TemporalType.TIMESTAMP)
	private Date horaEntrega;
	@Temporal(TemporalType.TIMESTAMP)
	private Date horaRealizacao;
	@Temporal(TemporalType.TIMESTAMP)
	private Date horaSaida;
	private BigDecimal total;
	@Enumerated(EnumType.STRING)
	private ETipoPedido tipoPedido;

	private BigDecimal desconto;

	private String tempoPreparo;

	@OneToMany(mappedBy = "pedido", targetEntity = ItemBebida.class, fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<ItemBebida> itemBebidas;

	private String observacao;

	public Pedido() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Cliente getCliente() {
		return cliente;
	}

	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}

	public Entregador getMotoboy() {
		return motoboy;
	}

	public void setMotoboy(Entregador motoboy) {
		this.motoboy = motoboy;
	}

	public Atendente getAtendente() {
		return atendente;
	}

	public void setAtendente(Atendente atendente) {
		this.atendente = atendente;
	}

	public ESituacaoPedido getSituacao() {
		return situacao;
	}

	public void setSituacao(ESituacaoPedido situacao) {
		this.situacao = situacao;
	}

	public List<Item> getItens() {
		return itens;
	}

	public void setItens(List<Item> itens) {
		this.itens = itens;
	}

	public Estabelecimento getEstabelecimento() {
		return estabelecimento;
	}

	public void setEstabelecimento(Estabelecimento estabelecimento) {
		this.estabelecimento = estabelecimento;
	}

	public FormaPagamento getFormaPagamento() {
		return formaPagamento;
	}

	public void setFormaPagamento(FormaPagamento formaPagamento) {
		this.formaPagamento = formaPagamento;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public Date getHoraEntrega() {
		return horaEntrega;
	}

	public void setHoraEntrega(Date horaEntrega) {
		this.horaEntrega = horaEntrega;
	}

	public Date getHoraRealizacao() {
		return horaRealizacao;
	}

	public void setHoraRealizacao(Date horaRealizacao) {
		this.horaRealizacao = horaRealizacao;
	}

	public Date getHoraSaida() {
		return horaSaida;
	}

	public void setHoraSaida(Date horaSaida) {
		this.horaSaida = horaSaida;
	}

	public BigDecimal getTotal() {
		return total;
	}

	public void setTotal(BigDecimal total) {
		this.total = total;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public ETipoPedido getTipoPedido() {
		return tipoPedido;
	}

	public void setTipoPedido(ETipoPedido tipoPedido) {
		this.tipoPedido = tipoPedido;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public List<ItemBebida> getItemBebidas() {
		return itemBebidas;
	}

	public void setItemBebidas(List<ItemBebida> itemBebidas) {
		this.itemBebidas = itemBebidas;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public String getTempoPreparo() {
		return tempoPreparo;
	}

	public void setTempoPreparo(String tempoPreparo) {
		this.tempoPreparo = tempoPreparo;
	}

}
