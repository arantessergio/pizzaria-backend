package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Cidade;

@Stateless
public class CidadeDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public CidadeDao() {
	}

	public void salvar(Cidade t) {
		if (t.getId() == null)
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		else
			this.alterar(t);
	}

	public void alterar(Cidade t) {
		this.manager.merge(t);
	}

	public void remover(Cidade t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Cidade> listar() {
		try {
			return this.manager.createQuery("select c from Cidade c",
					Cidade.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Cidade>();
		}
	}

	public Cidade buscarPorNome(String nome, Long idEstado) {
		try {
			return this.manager
					.createQuery(
							"select c from Cidade c where c.nome = :nome and c.estado.id = :idestado",
							Cidade.class).setParameter("nome", nome)
					.setParameter("idestado", idEstado).getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Cidade buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select c from Cidade c where c.id = :id",
							Cidade.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Cidade> buscarPorEstado(Long idEstado) {
		try {
			return this.manager
					.createQuery(
							"select c from Cidade c where c.estado.id = :id",
							Cidade.class).setParameter("id", idEstado)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Cidade>();
		}
	}

}
