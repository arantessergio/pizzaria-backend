package br.com.sergio.models;

public enum EStatus {

	INATIVO {
		@Override
		public String toString() {
			return "Inativo";
		}
	},
	ATIVO {
		@Override
		public String toString() {
			return "Ativo";
		}
	};

}
