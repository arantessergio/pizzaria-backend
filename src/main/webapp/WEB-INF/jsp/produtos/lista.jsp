<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Produtos</h2>
				<hr>
				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[ProdutosController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[ProdutosController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>
				<c:if test="${not empty codigoIgual}">
					<div class="alert alert-danger" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign"
							aria-hidden="true"></span> <span class="sr-only"></span> ${ codigoIgual }
					</div>
				</c:if>
				<c:if test="${not empty success}">
					<div class="alert alert-success" role="alert">
						<span class="glyphicon glyphicon-exclamation-sign"
							aria-hidden="true"></span> <span class="sr-only"></span> ${ success }
					</div>
				</c:if>
				<div class="table-responsive">
					<hr>
					<table class="table table-bordered">
						<tr>
							<th>Código</th>
							<th>Nome</th>
							<th>Descrição</th>
							<th>Valor custo</th>
							<th>Valor venda</th>
							<th>Situação</th>
							<th></th>
							<th></th>
						</tr>
						<c:forEach items="${ produtoList }" var="produto">
							<tr>
								<td><c:out value="${ produto.codigo }" /></td>
								<td><c:out value="${ produto.nome }" /></td>
								<td><c:out value="${ produto.descricao }" /></td>
								<td><c:out value="${ produto.precoCusto }" /></td>
								<td><c:out value="${ produto.precoVenda }" /></td>
								<td><c:out value="${ produto.situacao }" /></td>
								<td><a
									href="${ linkTo[ProdutosController].editar(produto.id) }"
									class="btn btn-info" type="button">Editar</a></td>
								<td><a
									href="${ linkTo[ProdutosController].solicitaexclusao(produto.id) }"
									class="btn btn-danger" type="button">Remover</a></td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>