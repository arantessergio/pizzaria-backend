package br.com.sergio.models;

public enum ETamanhoPizza {

	PEQUENA, MEDIA, GRANDE, EXTRA_GRANDE, EXTRA_GRANDE_BORDA;

}
