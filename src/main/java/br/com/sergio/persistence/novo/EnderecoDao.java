package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Endereco;

@Stateless
public class EnderecoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public EnderecoDao() {
	}

	public void salvar(Endereco t) {
		if (t.getId() == null)
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		else
			this.alterar(t);
	}

	public void alterar(Endereco t) {
		this.manager.merge(t);
	}

	public void remover(Endereco t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Endereco> listar() {
		try {
			return this.manager.createQuery("select e from Endereco e",
					Endereco.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Endereco>();
		}
	}

}
