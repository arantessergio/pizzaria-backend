package br.com.sergio.vos;

import java.util.List;

import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Estado;

public class CidadeVO {

	private Long id;
	private String ibge;
	private String nome;
	private Estado estado;
	private List<Bairro> bairros;

	public CidadeVO(Long id, String ibge, String nome, Estado estado,
			List<Bairro> bairros) {
		super();
		this.id = id;
		this.ibge = ibge;
		this.nome = nome;
		this.estado = estado;
		this.bairros = bairros;
	}

	public Long getId() {
		return id;
	}

	public String getIbge() {
		return ibge;
	}

	public String getNome() {
		return nome;
	}

	public Estado getEstado() {
		return estado;
	}

	public List<Bairro> getBairros() {
		return bairros;
	}

	public Cidade toCidade(CidadeVO vo) {
		Cidade c = new Cidade();
		c.setBairros(vo.getBairros());
		c.setEstado(vo.getEstado());
		c.setIbge(vo.getIbge());
		c.setId(vo.getId());
		c.setNome(vo.getNome());
		return c;
	}

}
