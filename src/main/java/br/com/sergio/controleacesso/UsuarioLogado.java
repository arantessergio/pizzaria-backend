package br.com.sergio.controleacesso;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import br.com.sergio.models.Pessoa;

@Named
@SessionScoped
public class UsuarioLogado implements Serializable {

	private static final long serialVersionUID = 2573354991351552886L;
	private Pessoa pessoa;

	public void loga(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public void desloga() {
		this.pessoa = null;
	}

	public boolean isLogado() {
		return this.pessoa != null;
	}

	public Pessoa getPessoa() {
		return this.pessoa;
	}

}
