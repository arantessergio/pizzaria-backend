<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Login</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/stylesheets/modal.css" />" rel="stylesheet">
<link href="<c:url value="/stylesheets/styles_login.css" />"
	rel="stylesheet">
</head>
<body>
	<div class="page-header">
		<h1>
			Pizza <small><i>express</i></small>
		</h1>
	</div>
	<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="text-center">Cadastre-se</h1>
				</div>
				<div class="modal-body">
					<form class="form col-md-12 center-block"
						action="${ linkTo[LoginController].cadastrar }">
						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="nome" id="txtNome" name="nome"
								ng-required="true"
								value="${ usuario.nome }">
						</div>
						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="telefone" id="txtTelefone" name="telefone"
								ng-required="true"
								value="${ usuario.telefone }">
						</div>
						Estado:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="cidade" name="estado" class="form-control">
									<c:forEach items="${ estados }" var="e">
										<option value="${ e.id }">${ e.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br /> Cidade:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="cidade" name="cidade" class="form-control">
									<c:forEach items="${ cidades }" var="c">
										<option value="${ c.id }">${ c.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br /> Bairro:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="cidade" name="bairro" class="form-control">
									<c:forEach items="${ bairros }" var="b">
										<option value="${ b.id }">${ b.nome }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br />
						<hr>
						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="rua" id="txtLogin" name="rua"
								ng-required="true"
								value="${ usuario.endereco.rua }">
						</div>
						<div class="form-group">
							<input type="number" class="form-control input-lg"
								placeholder="número" id="txtLogin" name="numero"
								ng-required="true"
								value="${ usuario.endereco.numero }">
						</div>
						<div class="form-group">
							<input type="text" class="form-control input-lg"
								placeholder="Complemento, AP, Fundos, etc..."
								id="txtComplemento" name="complemento"
								ng-required="true"
								value="${ usuario.endereco.complemento }">
						</div>
						<div class="form-group">
							<input type="email" class="form-control input-lg"
								placeholder="email" id="txtEmail" name="email"
								ng-required="true"
								value="${ usuario.email }">
						</div>
						<div class="form-group">
							<input type="password" class="form-control input-lg"
								placeholder="senha" id="txtPass" name="senha"
								ng-required="true"
								value="${ usuario.senha }">
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-lg btn-block botao-verde"
								type="submit" id="btnLogin">Cadastrar</button>
						</div>
					</form>
					<div ng-show="clienteForm.$invalid && clienteForm.nome.$dirty
					clienteForm.telefone.$dirty && clienteForm.rua.$dirty && clienteForm.numero.$dirty
					&& clienteForm.complemento.$dirty && clienteForm.email.$dirty &&
					clienteForm.senha.$dirty" class="alert alert-danger">
						Por favor preencha todos os campos, é rápido :D
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"/>"></script>
	<script src="<c:url value="/javascripts/bootstrap.min.js"/>"></script>
	<script src="<c:url value="/javascripts/angular.min.js"/>"></script>
</body>
</html>