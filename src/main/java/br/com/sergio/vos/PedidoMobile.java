package br.com.sergio.vos;


public class PedidoMobile {

	private String nomeCliente;
	private String emailCliente;
	private String token;
	private int quantidadePizza;
	private long[] idsSabores;
	private int tamanhoPizza;
	private int quantidadeBebidas;
	private int idBebida;
	private double latitude;
	private double longitude;
	private int idEstabelecimento;
	private int idFormaPagamento;

	public PedidoMobile(String nomeCliente, String emailCliente, String token,
			int quantidadePizza, long[] idsSabores, int tamanhoPizza,
			int quantidadeBebidas, int idBebida, double latitude,
			double longitude, int idEstabelecimento, int idFormaPagamento) {
		this.nomeCliente = nomeCliente;
		this.emailCliente = emailCliente;
		this.token = token;
		this.quantidadePizza = quantidadePizza;
		this.idsSabores = idsSabores;
		this.tamanhoPizza = tamanhoPizza;
		this.quantidadeBebidas = quantidadeBebidas;
		this.idBebida = idBebida;
		this.latitude = latitude;
		this.longitude = longitude;
		this.idEstabelecimento = idEstabelecimento;
		this.idFormaPagamento = idFormaPagamento;
	}

	public PedidoMobile() {
	}

	public String getNomeCliente() {
		return nomeCliente;
	}

	public String getEmailCliente() {
		return emailCliente;
	}

	public String getToken() {
		return token;
	}

	public int getQuantidadePizza() {
		return quantidadePizza;
	}

	public long[] getIdsSabores() {
		return idsSabores;
	}

	public int getTamanhoPizza() {
		return tamanhoPizza;
	}

	public int getQuantidadeBebidas() {
		return quantidadeBebidas;
	}

	public int getIdBebida() {
		return idBebida;
	}

	public double getLatitude() {
		return latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public int getIdEstabelecimento() {
		return idEstabelecimento;
	}

	public int getIdFormaPagamento() {
		return idFormaPagamento;
	}

}
