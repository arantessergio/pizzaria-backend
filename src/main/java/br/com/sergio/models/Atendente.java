package br.com.sergio.models;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;

@Entity
public class Atendente extends Pessoa implements Serializable {

    private static final long serialVersionUID = 6731240713846239422L;
    private boolean admin;
    @OneToMany(mappedBy = "atendente", targetEntity = Pedido.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Pedido> pedidos;
    @OneToMany(mappedBy = "atendente", targetEntity = Telefone.class, fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Telefone> telefones;
    @ManyToOne
    @JoinColumn(name = "estabelecimento_id")
    private Estabelecimento estabelecimento;

    public Atendente() {
    }

    public boolean isAdmin() {
	return admin;
    }

    public void setAdmin(boolean admin) {
	this.admin = admin;
    }

    public List<Pedido> getPedidos() {
	return pedidos;
    }

    public void setPedidos(List<Pedido> pedidos) {
	this.pedidos = pedidos;
    }

    public List<Telefone> getTelefones() {
	return telefones;
    }

    public void setTelefones(List<Telefone> telefones) {
	this.telefones = telefones;
    }

    public Estabelecimento getEstabelecimento() {
	return estabelecimento;
    }

    public void setEstabelecimento(Estabelecimento estabelecimento) {
	this.estabelecimento = estabelecimento;
    }

}
