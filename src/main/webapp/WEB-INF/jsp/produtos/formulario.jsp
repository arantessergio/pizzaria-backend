<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp"/>

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp"/>
			<div class="col-sm-9">
				<h2>Produtos</h2>
				<hr>

				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[ProdutosController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[ProdutosController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<form class="form-horizontal"
					action="${ linkTo[ProdutosController].salvar }" method="post">
					<fieldset>
						<input id="txtId" type="hidden" name="produto.id"
							value="${ produto.id }">
						<c:if test="${ not empty id_estabelecimento }">
							<input type="hidden" name="produto.estabelecimento.id"
								value="${ id_estabelecimento }">
						</c:if>
						<c:if test="${ not empty produto.estabelecimento.id }">
							<input type="hidden" name="produto.estabelecimento.id"
								value="${ produto.estabelecimento.id }">
						</c:if>
						<c:if test="${ not empty produto }">
							<input type="hidden" name="produto.id" value="${ produto.id }">
						</c:if>
						<div class="form-group">
							<div class="col-md-6">
								<input id="codigo" placeholder="Código"
									class="form-control input-md" required="required" type="number"
									name="produto.codigo" value="${ produto.codigo }">

							</div>
						</div>
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtNome" placeholder="Nome"
									class="form-control input-md" required="required" type="text"
									name="produto.nome" value="${ produto.nome }">

							</div>
						</div>

						<!-- Textarea -->
						<div class="form-group">
							<div class="col-md-4">
								<textarea class="form-control" id="txtDescricao"
									name="produto.descricao" required="required">${ produto.descricao }</textarea>
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtCusto" placeholder="R$ Custo"
									class="form-control input-md" required="required" type="text"
									name="produto.precoCusto" value="${ produto.precoCusto }">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtVenda" placeholder="R$ Venda"
									class="form-control input-md" required="required" type="text"
									name="produto.precoVenda" value="${ produto.precoVenda }">

							</div>
						</div>

						<br> Situação:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="situacao" name="produto.situacao"
									class="form-control">
									<option value="${ true }">Ativo</option>
									<option value="${ false }">Inativo</option>
								</select>
							</div>
						</div>

						<!-- Button (Double) -->
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
								<button id="btnCancelar" name="btnCancelar"
									class="btn btn-danger" type="reset">Cancelar</button>
							</div>
						</div>

					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
					<c:if test="${not empty success}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ success }
						</div>
					</c:if>

				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>