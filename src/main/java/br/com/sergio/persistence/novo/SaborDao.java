package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Sabor;

@Stateless
public class SaborDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public SaborDao() {
	}

	public void salvar(Sabor t) {
		if (t.getId() == null) {
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		} else {
			this.alterar(t);
		}
	}

	public void alterar(Sabor t) {
		this.manager.merge(manager.contains(t) ? t : manager.merge(t));
	}

	public void remover(Sabor t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Sabor> listar() {
		try {
			return this.manager.createQuery("select s from Sabor s",
					Sabor.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Sabor>();
		}
	}

	public List<Sabor> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select s from Sabor s where s.estabelecimento.id = :id",
							Sabor.class).setParameter("id", idEstabelecimento)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Sabor>();
		}
	}

	public Sabor buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select s from Sabor s where s.id = :id",
							Sabor.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
		}
		return null;
	}
}
