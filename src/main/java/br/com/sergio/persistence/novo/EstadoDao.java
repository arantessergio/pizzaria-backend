package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Estado;

@Stateless
public class EstadoDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public EstadoDao() {
	}

	public void salvar(Estado t) {
		if (t.getId() == null)
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		else
			this.alterar(t);
	}

	public void alterar(Estado t) {
		this.manager.merge(t);
	}

	public void remover(Estado t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<Estado> listar() {
		try {
			return this.manager.createQuery("select e from Estado e",
					Estado.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Estado>();
		}
	}

	public Estado buscarPorNome(String nome) {
		try {
			return this.manager
					.createQuery("select e from Estado e where e.nome = :nome",
							Estado.class).setParameter("nome", nome)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public Estado buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select e from Estado e where e.id = :id",
							Estado.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
