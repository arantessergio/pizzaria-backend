<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Login</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href="<c:url value="/stylesheets/modal.css" />" rel="stylesheet">
<link href="<c:url value="/stylesheets/styles_login.css" />"
	rel="stylesheet">
</head>
<body>
	<div class="page-header">
		<h1>
			Pizza <small>express</small>
		</h1>
	</div>
	<div id="loginModal" class="modal show" tabindex="-1" role="dialog"
		aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<h1 class="text-center">Login</h1>
				</div>
				<div class="modal-body">
					<form class="form col-md-12 center-block"
						action="${ linkTo[LoginController].logar }">
						<div class="form-group">
							<input type="email" class="form-control input-lg"
								placeholder="email" id="txtLogin" name="usuario.email"
								value="${ usuario.email }">
						</div>
						<div class="form-group">
							<input type="password" class="form-control input-lg"
								placeholder="Senha" id="txtSenha" name="usuario.senha"
								value="${ usuario.senha }">
						</div>
						<div class="form-group">
							<button class="btn btn-primary btn-lg btn-block botao-verde"
								type="submit" id="btnLogin">Acessar</button>
						</div>
						Ainda não é cadastrado?<br /> <a
							href="${ linkTo[LoginController].signup }">Clique aqui e
							cadastre-se agora mesmo!</a>
					</form>
					<div class="modal-footer">
						<c:if test="${not empty erro}">
							<div class="alert alert-danger" role="alert">${ erro }</div>
						</c:if>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script
		src="<c:url value="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"/>"></script>
	<script src="<c:url value="/javascripts/bootstrap.min.js"/>"></script>
</body>
</html>