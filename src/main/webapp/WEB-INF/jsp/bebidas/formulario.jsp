<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menulateral.jsp" />
			<div class="col-sm-9">
				<h2>Bebidas</h2>
				<hr>

				<div>
					<a id="btnNovo" class="btn btn-default"
						href="${ linkTo[BebidasController].formulario }">Novo</a> <a
						id="btnVisualizar" href="${ linkTo[BebidasController].lista }"
						class="btn btn-info">Visualizar</a>
				</div>

				<hr>

				<form class="form-horizontal"
					action="${ linkTo[BebidasController].salvar }" method="post">
					<fieldset>
						<input type="hidden" value="${ idEstabelecimento }"
							name="bebida.estabelecimento.id"> <input type="hidden"
							name="bebida.id" value="${ bebida.id }">
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtNome" placeholder="descrição"
									class="form-control input-md" required="required" type="text"
									name="bebida.descricao" value="${ bebida.descricao }">
							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtRg" placeholder="preço custo"
									class="form-control input-md" required="required" type="text"
									name="bebida.valorCusto" value="${ bebida.valorCusto }">

							</div>
						</div>

						<!-- Text input-->
						<div class="form-group">
							<div class="col-md-4">
								<input id="txtCpf" placeholder="preço venda"
									class="form-control input-md" required="required" type="text"
									name="bebida.valorVenda" value="${ bebida.valorVenda }">

							</div>
						</div>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Salvar</button>
							</div>
						</div>
					</fieldset>
				</form>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">
							<span class="glyphicon glyphicon-exclamation-sign"
								aria-hidden="true"></span> <span class="sr-only">Erro:</span> ${ erro }
						</div>
					</c:if>
				</div>
			</div>

		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>