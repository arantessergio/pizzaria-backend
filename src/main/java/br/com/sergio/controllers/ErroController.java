package br.com.sergio.controllers;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Result;

@Controller
public class ErroController {

	@Inject
	private Result result;
	
	public ErroController() {
	}

	public void paginaerro(String mensagem) {
		this.result.include("mensagem",
				"Ocorreu um erro ao tentar efetuar sua solicitação: \n"
						+ mensagem);
	}

}