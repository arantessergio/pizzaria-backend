package br.com.sergio.models;

public enum ETipoPedido {

	BALCAO {
		@Override
		public String toString() {
			return "Balcão";
		}
	},
	ENTREGA {
		@Override
		public String toString() {
			return "Entrega";
		}
	};

}
