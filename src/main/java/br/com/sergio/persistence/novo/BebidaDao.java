package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.Bebida;

@Stateless
public class BebidaDao {

	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public BebidaDao() {
	}

	public void salvar(Bebida b) {
		if (b.getId() == null)
			this.manager.persist(manager.contains(b) ? b : manager.merge(b));
		else
			this.alterar(b);
	}

	public void alterar(Bebida b) {
		this.manager.merge(b);
	}

	public void excluir(Bebida b) {
		this.manager.remove(manager.contains(b) ? b : manager.merge(b));
	}

	public List<Bebida> listar() {
		try {
			return this.manager.createQuery("select b from Bebida b",
					Bebida.class).getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Bebida>();
		}
	}

	public Bebida buscarPorId(Long id) {
		try {
			return this.manager
					.createQuery("select b from Bebida b where b.id = :id",
							Bebida.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

	public List<Bebida> listarPorEstabelecimento(Long idEstabelecimento) {
		try {
			return this.manager
					.createQuery(
							"select b from Bebida b where b.estabelecimento.id = :id",
							Bebida.class).setParameter("id", idEstabelecimento)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<Bebida>();
		}

	}

}
