package br.com.sergio.controllers;

import java.util.List;

import javax.inject.Inject;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Path;
import br.com.caelum.vraptor.Post;
import br.com.caelum.vraptor.Result;
import br.com.sergio.annotations.Transactional;
import br.com.sergio.controleacesso.UsuarioLogado;
import br.com.sergio.models.Atendente;
import br.com.sergio.models.Bairro;
import br.com.sergio.models.Cidade;
import br.com.sergio.models.Entregador;
import br.com.sergio.models.Estado;
import br.com.sergio.persistence.novo.BairroDao;
import br.com.sergio.persistence.novo.CidadeDao;
import br.com.sergio.persistence.novo.EnderecoDao;
import br.com.sergio.persistence.novo.EntregadorDao;
import br.com.sergio.persistence.novo.EstadoDao;

@Controller
public class EntregadoresController {

	private Result result;
	private EntregadorDao entregadorDao;
	private EstadoDao estadoDao;
	private CidadeDao cidadeDao;
	private BairroDao bairroDao;
	private EnderecoDao enderecoDao;
	private UsuarioLogado logado;

	@Inject
	public EntregadoresController(Result result, EntregadorDao entregadorDao,
			EstadoDao estadoDao, CidadeDao cidadeDao, BairroDao bairroDao,
			EnderecoDao enderecoDao, UsuarioLogado logado) {
		this.result = result;
		this.entregadorDao = entregadorDao;
		this.estadoDao = estadoDao;
		this.cidadeDao = cidadeDao;
		this.bairroDao = bairroDao;
		this.enderecoDao = enderecoDao;
		this.logado = logado;
	}

	public EntregadoresController() {
	}

	public void formulario() {
		this.result.include("bairros", bairroDao.listar());
		this.result.include("cidades", cidadeDao.listar());
		this.result.include("estados", estadoDao.listar());
		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();
		if (atendenteLogado != null) {
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendenteLogado.getNome();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
		}
	}

	@Post("/entregadores")
	@Transactional
	public void salvar(Entregador entregador) {

		String nomeEstado = entregador.getEndereco().getEstado().getNome();
		String nomeCidade = entregador.getEndereco().getCidade().getNome();
		String nomeBairro = entregador.getEndereco().getBairro().getNome();

		Estado estado = estadoDao.buscarPorNome(nomeEstado);
		Cidade cidade = cidadeDao.buscarPorNome(nomeCidade, estado.getId());
		Bairro bairro = bairroDao.buscarPorNome(nomeBairro, cidade.getId());

		entregador.getEndereco().setEstado(estado);
		entregador.getEndereco().setCidade(cidade);
		entregador.getEndereco().setBairro(bairro);

		this.enderecoDao.salvar(entregador.getEndereco());
		
		entregador.setEstabelecimento(((Atendente)this.logado.getPessoa()).getEstabelecimento());

		this.entregadorDao.salvar(entregador);
		this.result.include("sucesso", "Motoboy gravado com sucesso!");
		this.result.redirectTo(this).lista();
	}

	@Get("/entregadores")
	public List<Entregador> lista() {

		Atendente atendenteLogado = (Atendente) this.logado.getPessoa();

		if (atendenteLogado != null) {
			String nomeFantasia = atendenteLogado.getEstabelecimento()
					.getNomeFantasia();
			String nome = atendenteLogado.getNome();
			this.result.include("estabelecimento", nomeFantasia);
			this.result.include("nome", nome);
		}
		Long idEstabelecimento = atendenteLogado.getEstabelecimento().getId();
		return this.entregadorDao.listarPorEstabelecimento(idEstabelecimento);
	}

	@Path("/entregadores/solicitaexclusao/{id}")
	public void solicitaexclusao(Long id) {
		Entregador entregador = this.entregadorDao.buscarPorId(id);
		if (entregador != null) {
			this.result.include("id", entregador.getId());
			this.result.redirectTo(this).confirmaexclusao();
		} else {
			this.result.notFound();
		}
	}

	public void confirmaexclusao() {
	}

	@Path("/entregadores/excluir/{id}")
	public void excluir(Long id) {
		Entregador entregador = entregadorDao.buscarPorId(id);
		if (entregador != null) {
			this.entregadorDao.remover(entregador);
			this.result.include("sucess", "Item removido com sucesso!");
			this.result.redirectTo(this).lista();
		} else {
			this.result.notFound();
		}
	}

	@Get
	@Path(value = "/entregadores/{id}", priority = Path.LOW)
	public void edita(Long id) {
		Entregador entregador = this.entregadorDao.buscarPorId(id);
		if (entregador != null) {
			this.result.include("entregador", entregador);
			this.result.of(this).formulario();
		} else {
			this.result.notFound();
		}
	}

}
