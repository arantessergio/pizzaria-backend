<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page import="br.com.sergio.models.ETamanhoPizza"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
<meta charset="utf-8">
<title>Gestão pizzaria</title>
<meta name="generator" content="Bootply" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<link href="<c:url value="/stylesheets/bootstrap.min.css" />"
	rel="stylesheet">
<link href='<c:url value="/stylesheets/styles_index.css"/>'
	rel="stylesheet">
</head>
<body>

	<jsp:include page="../templates/barrasuperior.jsp" />

	<div class="container-fluid">
		<div class="row">
			<jsp:include page="../templates/menuclientes.jsp" />
			<div class="col-sm-9">
				<h2>Adicionar bebida</h2>
				<div class="modal-footer">
					<c:if test="${not empty erro}">
						<div class="alert alert-danger" role="alert">${ erro }</div>
					</c:if>
				</div>
				<form class="form-horizontal"
					action="${ linkTo[PedidosController].addBebida }" method="post">
					<fieldset>
						<br> Bebida:<br>
						<div class="form-group">
							<div class="col-md-4">
								<select id="sabor01" name="bebida" class="form-control">
									<c:forEach items="${ bebidas }" var="bebida">
										<option value="${ bebida.id }">${ bebida.descricao }</option>
									</c:forEach>
								</select>
							</div>
						</div>
						<br> Quantidade bebida:<br />
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtQuantidadeB" placeholder="Quantidade bebida"
									class="form-control input-md" required="required" type="number"
									name="quantidade" value="${ bebida.quantidade }">

							</div>
						</div>
						<br> Observação:<br />
						<div class="form-group">
							<div class="col-md-6">
								<input id="txtQuantidadeB" placeholder="Observação"
									class="form-control input-md" type="text"
									name="observacao" value="${ bebida.observacao }">

							</div>
						</div>
						<br>
						<div class="form-group">
							<div class="col-md-8">
								<button id="btnSalvar" name="btnSalvar" class="btn btn-success"
									type="submit">Adicionar</button>
							</div>
						</div>
					</fieldset>
				</form>
			</div>
		</div>
	</div>
	<script
		src="//ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
	<script src="/javascripts/bootstrap.min.js"></script>
	<script src="/javascripts/scripts_index.js"></script>
</body>
</html>