package br.com.sergio.persistence.novo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;

import br.com.sergio.models.ItemBebida;

@Stateless
public class ItemBebidaDao {
	@PersistenceContext(unitName = "pizzaria")
	private EntityManager manager;

	public ItemBebidaDao() {
	}

	public void salvar(ItemBebida t) {
		if (t.getId() == null) {
			this.manager.persist(manager.contains(t) ? t : manager.merge(t));
		} else {
			this.alterar(t);
		}
	}

	public void alterar(ItemBebida t) {
		this.manager.merge(t);
	}

	public void remover(ItemBebida t) {
		this.manager.remove(manager.contains(t) ? t : manager.merge(t));
	}

	public List<ItemBebida> listarPorPedido(Long idPedido) {
		try {
			return this.manager
					.createQuery(
							"select i from ItemBebida i where i.pedido.id = :id",
							ItemBebida.class).setParameter("id", idPedido)
					.getResultList();
		} catch (NoResultException e) {
			return new ArrayList<ItemBebida>();
		}
	}

	public ItemBebida buscarPorIdPedido(Long id) {
		try {
			return this.manager
					.createQuery(
							"select i from ItemBebida i where i.pedido.id = :id",
							ItemBebida.class).setParameter("id", id)
					.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}
	}

}
